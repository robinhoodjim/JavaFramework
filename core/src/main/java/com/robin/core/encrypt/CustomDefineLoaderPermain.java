package com.robin.core.encrypt;

import com.google.common.collect.Lists;
import com.robin.core.base.shell.CommandLineExecutor;
import com.robin.core.hardware.WinRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;
import java.math.BigInteger;
import java.security.ProtectionDomain;
import java.util.List;
import java.util.ResourceBundle;

public class CustomDefineLoaderPermain {
    static final byte[] m_datapadding = { 0x00 };
    static final String DUBS_PATH = "/var/lib/dbus/machine-id";
    static final String DUBS_PATH_EXT = "/etc/machine-id";
    static final List<String> EXEC_DARWIN = Lists.newArrayList(new String[]{"ioreg", "-rd1", "-c", "IOPlatformExpertDevice"});
    static String osName = System.getProperty("os.name").toLowerCase();

    public static void premain(String agentArgs, Instrumentation inst) {
        ResourceBundle bundle = ResourceBundle.getBundle("META-INF/encrypt");
                try{


            inst.addTransformer(new DefineClassLoaderTransform(bundle));

        }catch (Exception ex){

        }
    }
    static class DefineClassLoaderTransform implements ClassFileTransformer{
        ResourceBundle bundle;
        String keystr;
        byte[] mzheader=new byte[19];
        byte[] fileNamebytes=new byte[9];
        int[] XorKey = new int[8];
        private Logger logger= LoggerFactory.getLogger(DefineClassLoaderTransform.class);
        DefineClassLoaderTransform(ResourceBundle bundle){
            this.bundle=bundle;
            try (DataInputStream instream = new DataInputStream(ClassLoader.getSystemResourceAsStream("META-INF/config.bin"))){
                if(null!=instream){
                    instream.read(mzheader);
                    byte[] bytes=new byte[8];
                    instream.read(bytes);
                    for(int i=0;i<8;i++){
                        XorKey[i]=byteToInt(bytes[i]);
                    }
                    instream.read(m_datapadding);
                    keystr = decrypt(instream.readUTF(),XorKey);
                    instream.read(m_datapadding);
                    String machineId=getMachineId();
                    String header=machineId.substring(0,13);
                    String footer=machineId.substring(machineId.length()-13,machineId.length());
                    String xorPart=null;

                }
            }catch (IOException ex){
                logger.error("{}",ex);
            }
        }
        @Override
        public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
            return new byte[0];
        }
    }
    public static String getMachineId() throws RuntimeException {
        String machineId = null;
        BufferedReader reader = null;
        try {
            if (isWindows()) {
                machineId = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE, "SOFTWARE\\Microsoft\\Cryptography", "MachineGuid");
            } else if (isLinux()) {

                File file = new File(DUBS_PATH);
                if (!file.exists()) {
                    file = new File(DUBS_PATH_EXT);
                    if (!file.exists()) {
                        throw new RuntimeException("can not found suitable machineId");
                    }
                }
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                machineId = reader.readLine();
            } else if (isMacosName() || isMacosNameX()) {
                String output = executeCmd(EXEC_DARWIN);
                reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(output.getBytes())));
                String line;
                int pos = -1;
                while ((line = reader.readLine()) != null) {
                    pos = line.indexOf("IOPlatformUUID");
                    if (pos != -1) {
                        pos = line.indexOf("\" = \"");
                        machineId = line.substring(pos + 5, line.length()).trim();
                    }
                }
            }
            BigInteger val = new BigInteger(machineId.replaceAll("-", ""), 16);
            return val.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                if (null != reader) {
                    reader.close();
                }
            } catch (Exception ex) {

            }
        }

    }
    public static String executeCmd(List<String> cmd) throws Exception{
        String retStr=null;
        boolean runOk=true;
        try{
            ProcessBuilder builder=new ProcessBuilder(cmd);
            Process process=builder.start();
            CommandOutputThread thread=new CommandOutputThread(process,false);
            thread.start();
            int runCode=process.waitFor();
            thread.waitFor();
            if(runCode!=0 || !thread.isExecuteOk()){
                runOk=false;
            }
            retStr=thread.getOutput();
        }catch(Exception ex){
            throw ex;
        }
        if(!runOk){
            throw new RuntimeException("run script with error,output="+retStr);
        }
        return retStr;
    }
    static class CommandOutputThread extends Thread{
        private Process process;
        private StringBuilder builder=new StringBuilder();
        private boolean finished=false;
        //是否在后台显示日志
        private boolean enableOuptut=false;
        private boolean executeOk=true;
        public CommandOutputThread(Process process,boolean enableOutput){
            this.process=process;
            this.enableOuptut=enableOutput;
        }
        @Override
        public void run() {
            BufferedReader reader=new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader errreader=new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line;
            try{
                while((line=reader.readLine())!=null){
                    builder.append(line).append("\n");
                }
                while((line=errreader.readLine())!=null){

                    builder.append(line).append("\n");
                }
            }catch(Exception ex){
                executeOk=false;
            }finally{
                try{
                    if(reader!=null){
                        reader.close();
                    }
                    if(errreader!=null){
                        errreader.close();
                    }
                }catch(Exception ex){
                    executeOk=false;
                }
                finished=true;
            }
        }
        public String getOutput(){
            String ret="";
            if(builder.length()>0) {
                ret=builder.toString();
            }
            return ret;
        }
        public void waitFor() throws Exception{
            if(!finished) {
                Thread.sleep(100);
            }
        }
        public boolean isExecuteOk() {
            return executeOk;
        }

    }

    public static boolean isLinux() {
        return osName.indexOf("linux") >= 0;
    }

    public static boolean isMacosName() {
        return osName.indexOf("mac") >= 0 && osName.indexOf("osName") > 0 && osName.indexOf("x") < 0;
    }

    public static boolean isMacosNameX() {
        return osName.indexOf("mac") >= 0 && osName.indexOf("osName") > 0 && osName.indexOf("x") > 0;
    }

    public static boolean isWindows() {
        return osName.indexOf("windows") >= 0;
    }
    static String decrypt(String inputStr,int[] XorKey) {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < inputStr.length() / 2; i++) {
            int keypos = i % 8;
            int xorbyte = Integer.parseInt(
                    inputStr.substring(i * 2, (i + 1) * 2), 16)
                    ^ XorKey[keypos];
            builder.append((char) xorbyte);
        }
        return builder.toString();
    }
    static byte intToByte(int x) {
        return (byte) x;
    }
    static int byteToInt(byte b) {
        return b & 0xFF;
    }
}
