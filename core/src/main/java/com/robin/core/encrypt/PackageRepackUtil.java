package com.robin.core.encrypt;

import com.robin.core.hardware.MachineIdUtils;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ClassInfo;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ScanResult;

public class PackageRepackUtil {
    public static void repack(String basePackage,String outputPath,String machineId,Long expireTs){
        ScanResult scanResult=new ClassGraph().enableInterClassDependencies().enableAllInfo().whitelistPackages(basePackage).scan();

        ClassInfoList list=scanResult.getAllClasses();
        StringBuilder builder=new StringBuilder();
        for(ClassInfo classInfo:list){
            if(builder.length()>0){
                builder.delete(0,builder.length());
            }
            builder.append("package ").append(classInfo.getPackageName()).append(";\n");
            //import
            ClassInfoList importclasses=classInfo.getClassDependencies();
            for(ClassInfo importClazz:importclasses){
                builder.append("import ").append(classInfo.getPackageName()).append(".").append(classInfo.getName()).append(";\n");
            }
            if(classInfo.isPublic()){
                builder.append("public ");
            }
            if(classInfo.isAbstract()){
                builder.append(" abstract ");
            }
            if(classInfo.isFinal()){
                builder.append(" final ");
            }
            if(classInfo.isInterface()){
                builder.append(" interface ");
            }
            builder.append(classInfo.getName());


        }
    }
    public static void main(String[] args){
        PackageRepackUtil.repack("com.robin.core.base.dao","e:/tmp/output", MachineIdUtils.getMachineId(),System.currentTimeMillis());
    }
}
