package com.robin.core.query.mapper.segment;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;
import java.util.Map;

public abstract class SqlOperationSegment extends AbstractSegment {
    protected List<AbstractSegment> segments;
    protected String classType;

    public SqlOperationSegment(String nameSpace, String id, String value, List<AbstractSegment> segments) {
        super(nameSpace, id, value);
        this.segments=segments;
    }
    public SqlOperationSegment(String nameSpace, String id, String value, List<AbstractSegment> segments,String classType) {
        super(nameSpace, id, value);
        this.segments=segments;
        this.classType=classType;
    }

    @Override
    public String getSqlPart(Map<String, Object> params, Map<String, ImmutablePair<String, List<AbstractSegment>>> segmentsMap) {
        StringBuilder builder=new StringBuilder();
        for(AbstractSegment segment:segments){
            builder.append(segment.getSqlPart(params,segmentsMap));
        }
        return builder.toString();
    }
}
