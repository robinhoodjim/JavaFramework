package com.robin.core.query.mapper.segment;

import java.util.List;

public class UpdateSegment extends SqlOperationSegment {
    public UpdateSegment(String nameSpace, String id, String value, List<AbstractSegment> segments) {
        super(nameSpace, id, value, segments);
    }

    public UpdateSegment(String nameSpace, String id, String value, List<AbstractSegment> segments, String classType) {
        super(nameSpace, id, value, segments, classType);
    }
}
