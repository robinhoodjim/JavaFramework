package com.robin.core.base.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

@FunctionalInterface
public interface IQueryCondition<T> {
    void wrapCondition(QueryWrapper<T> queryWrapper, Object object);
}
