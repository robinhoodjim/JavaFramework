package com.robin.core.test.db;

import com.robin.core.base.dao.SimpleJdbcDao;
import com.robin.core.base.datameta.*;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * <p>Project:  frame</p>
 *
 * <p>Description: TestMeta </p>
 *
 * <p>Copyright: Copyright (c) 2021 modified at 2021-09-16</p>
 *
 * <p>Company: seaboxdata</p>
 *
 * @author luoming
 * @version 1.0
 */
public class TestMeta {
    @Test
    public void testMeta() throws SQLException{
        DataBaseParam param=new DataBaseParam("jdbc:mysql://62.234.123.77:3506/ksxcdb?useUnicode=true&characterEncoding=UTF-8&rewriteBatchedStatements=true","root","Dfjx@1243");
        BaseDataBaseMeta meta= DataBaseMetaFactory.getDataBaseMetaByType(BaseDataBaseMeta.TYPE_MYSQL,param);
        try(Connection connection= SimpleJdbcDao.getConnection(meta)){
            List<DataBaseColumnMeta> list= DataBaseUtil.getTableMetaByTableName(connection,"yqfkglpt_no_matching_phone","epidemic",BaseDataBaseMeta.TYPE_MYSQL);
            System.out.println(DataBaseUtil.getTablePrivileges(connection,"t_test",null));
            System.out.println(SimpleJdbcDao.queryBySql(connection,"select * from t_test",new Object[]{}));
        }catch (SQLException ex){
            throw ex;
        }
    }
}
