package com.robin.core.test.util;

import com.robin.core.base.util.StringUtils;
import junit.framework.TestCase;
import org.junit.Test;

/**
 * <p>Created at: 2019-08-20 09:32:26</p>
 *
 * @author robinjim
 * @version 1.0
 */
public class TestMd5 extends TestCase {
    @Test
    public void testMd5Gen() throws Exception{
        String password="123456";
        String encryStr= StringUtils.getMd5Encry(password);
        System.out.println(encryStr);
        assertNotNull(encryStr);
        String na1="getNsasaTR";
        System.out.println(na1.substring(3,4).toLowerCase()+na1.substring(4));
    }
}
