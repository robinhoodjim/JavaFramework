package com.robin.core.test.util;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileType;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * <p>Created at: 2019-08-21 12:40:01</p>
 *
 * @author robinjim
 * @version 1.0
 */
public class TestGetJarContent {
    @Test
    public void test() throws Exception{
        StandardFileSystemManager manager=new StandardFileSystemManager();
        manager.init();
        FileObject obj=manager.resolveFile("jar:file:/E:/dev/workspacenew/JavaFrame/metadata/target/metadata-1.0-SNAPSHOT.jar!/BOOT-INF/classes/queryConfig");
        System.out.println(obj.getType());
        if(obj.getType().equals(FileType.FOLDER)){
            FileObject[] files=obj.getChildren();
            for(FileObject o:files){
                System.out.println(o.getName());
            }
        }

    }
    @Test
    public void test1() throws Exception{
        List<String> files= IOUtils.readLines(this.getClass().getClassLoader().getResourceAsStream("query"), Charsets.UTF_8);
        for(String file:files){
            InputStream stream=this.getClass().getClassLoader().getResourceAsStream("query/"+file);
            System.out.println(stream);
        }
        System.out.println(files);
    }
}
