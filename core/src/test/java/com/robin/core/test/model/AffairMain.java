package com.robin.core.test.model;

import com.robin.core.base.annotation.MappingEntity;
import com.robin.core.base.annotation.MappingField;
import com.robin.core.base.model.BaseObject;
import lombok.Data;

@MappingEntity(table = "t_affair" )
@Data
public class AffairMain extends BaseObject {
    @MappingField(increment = true,primary = true)
    private Long id;
    @MappingField
    private Long deptId;
    @MappingField
    private String name;
    @MappingField
    private Long parentId;
    @MappingField
    private Long tenantId;
    @MappingField
    private Long creator;
    @MappingField
    private Long modifier;
    @MappingField
    private Long createTime;
    @MappingField
    private Long modifyTime;




}
