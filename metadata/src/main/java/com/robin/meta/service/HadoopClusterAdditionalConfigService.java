package com.robin.meta.service;

import com.robin.core.base.service.BaseAnnotationJdbcService;
import com.robin.meta.model.HadoopClusterAdditionalConfig;
import org.springframework.stereotype.Component;


@Component
public class HadoopClusterAdditionalConfigService extends BaseAnnotationJdbcService<HadoopClusterAdditionalConfig,Long> {
}
