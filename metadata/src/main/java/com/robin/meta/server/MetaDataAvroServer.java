package com.robin.meta.server;

import com.robin.core.fileaccess.util.AvroUtils;
import com.robin.meta.service.resource.GlobalResourceService;
import org.apache.avro.ipc.NettyServer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;

/**
 * <p>Project:  frame</p>
 * <p>
 * <p>Description:com.robin.meta.server</p>
 * <p>
 * <p>Copyright: Copyright (c) 2018 create at 2018年11月06日</p>
 * <p>
 * <p>Company: zhcx_DEV</p>
 *
 * @author robinjim
 * @version 1.0
 */
@Component
public class MetaDataAvroServer implements InitializingBean {
    private int port=7899;

    @Autowired
    private GlobalResourceService globalResourceService;


    @Override
    public void afterPropertiesSet() throws Exception {
        MetaDataAvroRestService responder=new MetaDataAvroRestService(AvroUtils.parseProtocolWithClassPath("interface.avro"));
        NettyServer server=new NettyServer(responder,new InetSocketAddress(port));
        server.start();
    }

    public void setPort(int port) {
        this.port = port;
    }
}
