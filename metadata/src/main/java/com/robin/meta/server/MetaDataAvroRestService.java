package com.robin.meta.server;

import com.robin.core.base.spring.SpringContextHolder;
import com.robin.core.fileaccess.meta.DataCollectionMeta;
import com.robin.meta.service.resource.GlobalResourceService;
import org.apache.avro.Protocol;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.ipc.generic.GenericResponder;
import org.springframework.util.Assert;

/**
 * <p>Project:  frame</p>
 * <p>
 * <p>Description:com.robin.meta.server</p>
 * <p>
 * <p>Copyright: Copyright (c) 2018 create at 2018年11月07日</p>
 * <p>
 * <p>Company: </p>
 *
 * @author robinjim
 * @version 1.0
 */
public class MetaDataAvroRestService extends GenericResponder {
    private final Protocol protocol;
    private int port;
    public MetaDataAvroRestService(Protocol local) {
        super(local);
        this.protocol=local;
    }
    public MetaDataAvroRestService(Protocol protocol,int port){
        super(protocol);
        this.protocol=protocol;
        this.port=port;
    }

    @Override
    public Object respond(Protocol.Message message, Object request) throws Exception {
        GenericRecord record= (GenericRecord) request;
        GenericRecord retrecord=null;
        GlobalResourceService service=  SpringContextHolder.getBean(GlobalResourceService.class);
        if("queryMetaMsg".equals(message.getName())){
            GenericRecord msg= (GenericRecord) record.get("reqMeta");
            Assert.notNull(msg.get("dataSourceId"),"dataSourceId is null");
            Long sourceId= (Long) msg.get("dataSourceId");
            String selectSource=(null!=msg.get("selectSource"))?msg.get("selectSource").toString():null;
            retrecord=new  GenericData.Record(protocol.getType("metaQueryResp"));
            DataCollectionMeta collectionMeta=service.getResourceMetaDef(sourceId.toString()+","+selectSource);
            retrecord.put("dataSourceId",sourceId);
            retrecord.put("avroSchema",service.getDataSourceSchemaDesc(collectionMeta,sourceId,selectSource,0));
        }
        return retrecord;
    }
}
