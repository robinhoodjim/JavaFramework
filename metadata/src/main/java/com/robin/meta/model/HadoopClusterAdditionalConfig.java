package com.robin.meta.model;

import com.robin.core.base.annotation.MappingEntity;
import com.robin.core.base.annotation.MappingField;
import com.robin.core.base.model.BaseObject;
import lombok.Data;

/**
 * <p>Project:  frame</p>
 * <p>
 * <p>Description:com.robin.meta.cache</p>
 * <p>
 * <p>Copyright: Copyright (c) 2018 create at 2018年11月09日</p>
 * <p>
 * <p>Company: zhcx_DEV</p>
 *
 * @author robinjim
 * @version 1.0
 */
@MappingEntity(table = "t_hadoop_cluster_addconfig")
@Data
public class HadoopClusterAdditionalConfig extends BaseObject {
    @MappingField(primary = true,increment = true)
    private Long id;
    @MappingField(field = "cluster_id")
    private Long clusterId;
    @MappingField(field = "config_key")
    private String key;
    @MappingField(field = "config_value")
    private String value;

}
