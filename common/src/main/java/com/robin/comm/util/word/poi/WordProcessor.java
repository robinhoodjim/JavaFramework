package com.robin.comm.util.word.poi;


import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hwpf.model.PicturesTable;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.usermodel.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.*;

@Slf4j
public class WordProcessor {
    public static WordContent readDoc(InputStream stream,String fileSuffix){
        boolean is2007=WordConst.TYPE_WORD2007.equalsIgnoreCase(fileSuffix);
        List<String> contentList=new ArrayList<>();
        WordContent content=new WordContent();
        if(is2007){
            try(XWPFDocument doc=new XWPFDocument(stream)){
                int tablePos=0;
                Iterator<IBodyElement> elementList=doc.getBodyElementsIterator();
                while(elementList.hasNext()){
                    IBodyElement element=elementList.next();
                    BodyElementType type= element.getElementType();
                    switch (type) {
                        case CONTENTCONTROL:
                            break;
                        case PARAGRAPH:
                            XWPFParagraph paragraph= (XWPFParagraph)element;
                            if(!StringUtils.isEmpty(paragraph.getParagraphText())) {
                                contentList.add(paragraph.getParagraphText());
                            }
                            break;

                        case TABLE:
                            XWPFTable table=(XWPFTable) element;
                            List<XWPFTableRow> rows=table.getRows();
                            List<String> columnNames=new ArrayList<>();
                            List<Map<String,String>> rsList=new ArrayList<>();
                            if(!CollectionUtils.isEmpty(rows)){
                                if(rows.size()>1){
                                    XWPFTableRow toprow= rows.get(0);
                                    //get frist row as Map columnName
                                    for(XWPFTableCell cell:toprow.getTableCells()){
                                        if(!StringUtils.isEmpty(cell.getText())){
                                            columnNames.add(cell.getText());
                                        }
                                    }
                                    for(int i=1;i<rows.size();i++){
                                        Map<String,String> tmpMap=new HashMap<>();
                                        List<XWPFTableCell> cells=rows.get(i).getTableCells();
                                        for(int j=0;j<columnNames.size();j++){
                                            tmpMap.put(columnNames.get(j),cells.get(j).getText());
                                        }
                                        rsList.add(tmpMap);
                                    }
                                }else{
                                    //table has only one row,Key change to Position
                                    List<XWPFTableCell> cells=rows.get(0).getTableCells();
                                    Map<String,String> tmpMap=new HashMap<>();
                                    for(int i=0;i<cells.size();i++){
                                        tmpMap.put(String.valueOf(i),cells.get(i).getText());
                                    }
                                    rsList.add(tmpMap);
                                }
                                content.addTableMap(tablePos++,rsList);
                            }
                            break;
                        default:
                            log.warn(" word content type {} not support!",type);
                            break;
                    }
                }
                content.setContents(contentList);
                List<XWPFPictureData> pics= doc.getAllPictures();
                if(!CollectionUtils.isEmpty(pics)){
                    for(XWPFPictureData pictureData:pics){
                        String fileName=pictureData.getFileName();
                        String extension=pictureData.suggestFileExtension();
                        content.addPicMap(fileName,pictureData.getData());
                    }
                }
            }catch (IOException ex){
                ex.printStackTrace();
            }

        }else{
            try(POIFSFileSystem fs=new POIFSFileSystem(stream)){
                HWPFDocument hwpf=new HWPFDocument(fs);
                WordExtractor extractor=new WordExtractor(hwpf);
                String[] strcontent=extractor.getParagraphText();
                PicturesTable picturesTable= hwpf.getPicturesTable();
                List<Picture> pictures= picturesTable.getAllPictures();

            }catch (IOException ex){

            }

        }
        return  content;
    }
    public static XWPFDocument processWithTemplate(InputStream docinput,Map<String,String> paramMap) throws Exception{
        XWPFDocument document=null;
        try{
            document=new XWPFDocument(docinput);
            traverse(document,paramMap);
        }catch (Exception ex){
            throw ex;
        }
        return document;
    }
    public static void traverse(XWPFDocument document,Map<String,String> paramMap) {
        //获取文档中各段落集合
        List<XWPFParagraph> paragraphs = document.getParagraphs();
        //遍历各段落
        for (XWPFParagraph paragraph : paragraphs) {
            List<XWPFRun> runs = paragraph.getRuns();
            for (XWPFRun run : runs) {
                // TODO 处理文本
                run.setText(changeValue(run.toString(),paramMap),0);
            }
        }
        List<XWPFTable> tables=document.getTables();
        if(!CollectionUtils.isEmpty(tables)){
            for(XWPFTable table:tables){
                for(int i=0;i<table.getRows().size();i++){
                    XWPFTableRow row=table.getRow(i);
                    List<XWPFTableCell> cells=row.getTableCells();
                    for(XWPFTableCell cell:cells){
                        List<XWPFParagraph> paragraphs1 = cell.getParagraphs();
                        if(paragraphs.size()>1){
                            for (XWPFParagraph paragraph : paragraphs1) {
                                List<XWPFRun> runs = paragraph.getRuns();
                                for (XWPFRun run : runs) {
                                    // TODO 处理文本
                                    run.setText(changeValue(run.toString(),paramMap),0);
                                }
                            }
                        }else{
                            String value=cell.getText();
                            value=changeValue(value,paramMap);
                            cell.setText(value);
                        }

                    }
                }
            }
        }
    }
    public static String changeValue(String value, Map<String, String> textMap) {
        Set<Map.Entry<String, String>> textSets = textMap.entrySet();
        for (Map.Entry<String, String> textSet : textSets) {
            //匹配模板与替换值 格式${key}
            String key = "${" + textSet.getKey() + "}";
            String regex="\\$\\{"+textSet.getKey() + "\\}";
            if (value.indexOf(key) != -1) {
                value = value.replaceAll(regex,textSet.getValue());
            }
        }
        //模板未匹配到区域替换为空

        return value;
    }
    public static void main(String[] args){
        try(InputStream inputStream=new FileInputStream(new File("e:/cityName.docx"))){
            Map<String,String> paramMap=new HashMap<>();
            paramMap.put("cityName","南宁市");
            paramMap.put("dayStr","8月2日");
            paramMap.put("num1","10");
            paramMap.put("num2","11");
            paramMap.put("num3","12");
            paramMap.put("num4","13");
            paramMap.put("num5","14");
            paramMap.put("num6","15");
            paramMap.put("num7","16");
            paramMap.put("num8","17");
            paramMap.put("num9","18");
            paramMap.put("num10","19");
            paramMap.put("num11","20");
            XWPFDocument document=processWithTemplate(inputStream,paramMap);
            FileOutputStream out=new FileOutputStream(new File("e:/test2.docx"));
            document.write(out);
            out.flush();
            out.close();
            document.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
