package com.robin.comm.util.word.poi;


import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import java.io.IOException;
import java.io.OutputStream;

public class WordPoiUtils {
    public static void write(XWPFDocument document,OutputStream out) throws IOException {
        document.write(out);
    }
    public static void insertContent(XWPFDocument document,WordConfig config, String content){
        XWPFParagraph paragraph=document.createParagraph();
        paragraph.setAlignment(config.getAlignment());
        paragraph.setFirstLineIndent(config.getIndentation());
        XWPFRun runtitle=paragraph.createRun();
        runtitle.setFontSize(config.getFontSize());
        runtitle.setFontFamily(config.getFontName());

    }
}
