package com.robin.comm.util.jdbc;

import com.robin.core.base.datameta.BaseDataBaseMeta;
import com.robin.core.base.datameta.DataBaseTypeEnum;
import com.robin.core.convert.util.DataTypeEnum;
import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.util.ObjectUtils;

import javax.sql.DataSource;
import java.io.File;
import java.net.URL;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


public class DynamicJdbcLoader implements DisposableBean {
    private static final Map<DataBaseTypeEnum, Driver> DB_DRIVER_MAP=new ConcurrentHashMap<>();
    private static final Map<DataBaseTypeEnum, Class<Driver>> DB_DRIVERCLASS_MAP=new ConcurrentHashMap<>();
    private static final Map<DataBaseTypeEnum,File> JAR_FILE_MAP=new ConcurrentHashMap<>();
    private static final Map<DataBaseTypeEnum, DynamicJdbcClassLoader> URL_CLASS_LOADER_MAP = new ConcurrentHashMap();
    private static final Map<String, DataSource> CONNECTION_POLL_MAP = new ConcurrentHashMap();
    private ReadWriteLock locker = null;

    private ResourceBundle bundle;
    private String jdbcFileBasePath;
    private static final String JAR_NAME_FORMAT = "%s-%s.jar";

    public DynamicJdbcLoader(String bundleName){
        bundle=ResourceBundle.getBundle(bundleName);
        locker=new ReentrantReadWriteLock();
        if(bundle.containsKey("jdbcFileBasePath")){
            jdbcFileBasePath=bundle.getString("jdbcFileBasePath");
        }else{
            jdbcFileBasePath=System.getProperty("java.io.tmpdir");
        }

        DataBaseTypeEnum[] enums= DataBaseTypeEnum.class.getEnumConstants();
        for(DataBaseTypeEnum dbEnum:enums){
            String jarName = String.format("%s-%s.jar", dbEnum.getCode(), dbEnum.getVersion());
            try{
                File file=new File(jdbcFileBasePath+File.separator+jarName);
                if(file.exists()){
                    JAR_FILE_MAP.put(dbEnum,file);
                }
            }catch (Exception ex){

            }

        }

    }
    private void dynamicLoad(DataBaseTypeEnum typeEnum){
        this.locker.readLock().lock();
        try{
            DriverShim driver=null;
            this.locker.readLock().unlock();
            //锁降级
            this.locker.writeLock().lock();
            this.locker.readLock().lock();
            driver=(DriverShim) DB_DRIVER_MAP.get(typeEnum);
            if(ObjectUtils.isEmpty(driver)){
                File file=JAR_FILE_MAP.get(typeEnum);
                if(file==null){

                }
                DynamicJdbcClassLoader classLoader=new DynamicJdbcClassLoader(new URL[]{new URL("jar:" + file.toURI().toURL().toExternalForm() + "!/")},findParentClassLoader());
                Class<Driver> clazz=(Class<Driver>)Class.forName(typeEnum.getDrivers(),true,classLoader);
                driver=new DriverShim((Driver)clazz.newInstance());
                DriverManager.registerDriver(driver);
                DriverManager.setLoginTimeout(10);
                DB_DRIVER_MAP.put(typeEnum,driver);
                DB_DRIVERCLASS_MAP.put(typeEnum,clazz);
                URL_CLASS_LOADER_MAP.put(typeEnum,classLoader);

            }
        }catch (Exception ex){

        }finally {
            this.locker.writeLock().unlock();
            this.locker.readLock().unlock();
        }

    }
    private static ClassLoader findParentClassLoader() {
        ClassLoader parent = DynamicJdbcLoader.class.getClassLoader();
        if (parent == null) {
            parent = DynamicJdbcLoader.class.getClassLoader();
        }

        if (parent == null) {
            parent = ClassLoader.getSystemClassLoader();
        }

        return parent;
    }

    private String loadPath;
    public DataSource getSourceByMeta(DataBaseTypeEnum typeEnum, HikariConfig config){
        if(!DB_DRIVER_MAP.containsKey(typeEnum)){
            dynamicLoad(typeEnum);
        }else{
            try{
                

            }catch (Exception ex){

            }

        }
        return null;
    }

    @Override
    public void destroy() throws Exception {

    }
}
