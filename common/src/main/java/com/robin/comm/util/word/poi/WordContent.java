package com.robin.comm.util.word.poi;


import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class WordContent {   
    private List<String> contents=new ArrayList<>();
    private List<Byte[]> pics=new ArrayList<>();
    private Map<Integer,List<Map<String,String>>> tableMaps=new HashMap<>();
    private Map<String,byte[]> picMap=new HashMap<>();
    public WordContent(){
    }
    public void addTableMap(Integer pos,List<Map<String,String>> rsMap){
        tableMaps.put(pos,rsMap);
    }
    public void addPicMap(String fileName,byte[] content){
        picMap.put(fileName,content);
    }
       
    
}
