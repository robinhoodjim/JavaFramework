package com.robin.comm.util.word.poi;


import lombok.Data;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;

@Data
public class WordConfig {
    private ParagraphAlignment alignment;
    private String fontName="STSong-Light";
    private Integer fontSize=10;
    private Integer indentation=400;

}
