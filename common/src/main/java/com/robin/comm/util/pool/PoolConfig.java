package com.robin.comm.util.pool;


public class PoolConfig {
    private String driverClassName;
    private String jdbcUrl;
    private String password;
    private String username;
    private String connectionTestQuery="SELECT 1";
    private int maxPoolSize;
    private int minIdle;
    private long idleTimeout;

}
