package com.robin.comm.util.jdbc;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;


@Slf4j
public class DynamicJdbcClassLoader extends URLClassLoader {
    private JarURLConnection cachedJarFile = null;
    public DynamicJdbcClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
        try {
            if (!ObjectUtils.isEmpty(urls)) {
                URLConnection uc = urls[0].openConnection();
                if(JarURLConnection.class.isAssignableFrom(uc.getClass())){
                    uc.setUseCaches(true);
                    ((JarURLConnection)uc).getManifest();
                    this.cachedJarFile = (JarURLConnection)uc;
                }
            }
        }catch (Exception ex){

        }
    }
    public void unload(){
        if (!ObjectUtils.isEmpty(cachedJarFile)) {
            try {
                log.info("卸载JAR文件：" + cachedJarFile.getJarFile().getName());
                cachedJarFile.getJarFile().close();
            } catch (Exception var3) {
                log.error(var3.getMessage(), var3);
            }
        }
    }
}
