/*
 * Copyright (c) 2015,robinjim(robinjim@126.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.robin.comm.util.mock;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.robin.comm.util.config.YamlUtils;
import com.robin.core.base.dao.SimpleJdbcDao;
import com.robin.core.base.datameta.*;
import com.robin.core.base.util.Const;
import com.robin.core.base.util.StringUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Slf4j
public class MockDataUtils {
    private static Map<String, List<String>> dictMap = new HashMap<>();
    private static final DecimalFormat numberFormat = new DecimalFormat("#.######");
    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final Gson gson = new Gson();

    public static void main(String[] args) {
        try {
            mockTableData(args[0], new FileOutputStream(new File(args[1])), 1234567L);
        } catch (Exception ex) {
            log.error("{}", ex.getMessage());
        }
    }

    public static void mockTableData(String configFile, OutputStream output, Long randomSeed) throws Exception {
        Map<String, Object> map = YamlUtils.parseYamlFileWithClasspath(configFile, MockDataUtils.class);

        int port = 0;
        if (map.containsKey("port")) {
            port = Integer.parseInt(map.get("port").toString());
        }


        DataBaseParam param = (map.containsKey("db.version")) ? new DataBaseParam(map.get("db.hostName").toString(), port, map.get("db.schema").toString(), map.get("db.userName").toString(), map.get("db.password").toString(), Integer.valueOf(map.get("db.version").toString())) : new DataBaseParam(map.get("db.hostName").toString(), port, map.get("db.schema").toString(), map.get("db.userName").toString(), map.get("password").toString());
        BaseDataBaseMeta meta = DataBaseMetaFactory.getDataBaseMetaByType(map.get("dbType").toString(), param);
        if (map.containsKey("db.jdbcUrl")) {
            param.setUrl(map.get("db.jdbcUrl").toString());
        } else {
            param.setUrl(param.getUrlByMeta(meta));
        }
        String split = (map.containsKey("output.split")) ? map.get("output.split").toString() : ",";

        List<DataBaseColumnMeta> metaList = DataBaseUtil.getTableMetaByTableName(SimpleJdbcDao.getConnection(meta), map.get("db.tableName").toString(), null, null);
        Map<String, DataBaseColumnMeta> map1 = metaList.stream().collect(Collectors.toMap(DataBaseColumnMeta::getColumnName, Function.identity()));
        List<DataBaseColumnMeta> mList = new ArrayList<>();
        Set<String> names = new HashSet<>();
        for (int i = 0; i < metaList.size() - 1; i++) {
            DataBaseColumnMeta colmeta = metaList.get(i);
            if (!names.contains(colmeta.getColumnName())) {
                names.add(colmeta.getColumnName());
                mList.add(colmeta);
            }
        }

        Map<String, MetaCfg> columnCfg = navigateProp(map, map1);
        long genRows = Long.parseLong(map.get("output.mockRows").toString());

        Random random = new Random(randomSeed);
        StringBuilder builder = new StringBuilder();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output));
        try {
            log.info("start at {}", new Date());
            if (genRows > 0) {
                for (int i = 0; i < genRows - 1; i++) {
                    mockRows(columnCfg, mList, random, builder, Long.valueOf(i), split);
                    writer.write(builder.substring(0, builder.length() - 1) + "\n");
                    builder.delete(0, builder.length());
                    if (i % 10000 == 0) {
                        writer.flush();
                    }
                }
            }
        } catch (Exception ex) {
            log.error("{}", ex.getMessage());
        } finally {
            writer.close();
            log.info("end at {}", new Date());
        }
    }

    public static Map<String, MetaCfg> navigateProp(Map<String, Object> paramMap, Map<String, DataBaseColumnMeta> columnMetaMap) {
        Iterator<Map.Entry<String, Object>> iter = paramMap.entrySet().iterator();
        Map<String, MetaCfg> map = new HashMap<>();
        while (iter.hasNext()) {
            Map.Entry<String, Object> entry = iter.next();
            if (entry.getKey().startsWith("COLUMPROP")) {
                String columnName = entry.getKey().substring(10, entry.getKey().length());
                String cfg = entry.getValue().toString();
                if (columnMetaMap.containsKey(columnName)) {
                    MetaCfg metaCfg = gson.fromJson(cfg, new TypeToken<MetaCfg>() {
                    }.getType());
                    if (metaCfg.getStrCfg()!=null && metaCfg.getStrCfg().toLowerCase().startsWith("dict")) {
                        String sql = metaCfg.getStrCfg().substring(5, metaCfg.strCfg.length());
                        //List<Map<String,Object>> list = SimpleJdbcDao.queryBySql(conn, sql,new Object[]{null});
                        List<String> vList = new ArrayList<>();

                        dictMap.put(columnName, vList);
                    }
                    map.put(columnName, metaCfg);
                }
            }
        }
        return map;
    }

    public static void mockRows(Map<String, MetaCfg> cfgMap, List<DataBaseColumnMeta> metaList, Random random, StringBuilder builder, Long rows, String split) {
        for (int i = 0; i < metaList.size(); i++) {
            //自增长字段不需要给值
            if (!metaList.get(i).isIncrement()) {
                mockDataWithType(metaList.get(i), cfgMap.get(metaList.get(i).getColumnName()), rows + 1, random, builder, rows, split);
            } else {
                builder.append(split);
            }
        }
    }

    public static void mockDataWithType(DataBaseColumnMeta meta, MetaCfg cfg, Long pos, Random random, StringBuilder builder, Long rows, String split) {

        if (meta.isIncrement()) {
            builder.append(",");
        } else {
            if (cfg != null && "enum".equalsIgnoreCase(cfg.getGenType())) {
                String[] arr = cfg.getStrCfg().split(",");
                builder.append(arr[random.nextInt(arr.length)]).append(split);
            } else if (meta.getColumnType().toString().equals(Const.META_TYPE_INTEGER) || meta.getColumnType().toString().equals(Const.META_TYPE_BINARY)) {
                int range = 10000;
                int start = 0;
                if (cfg != null && cfg.getMaxRange() != null && cfg.getMinRange() != null) {
                    range = Integer.parseInt(cfg.getMaxRange()) - Integer.parseInt(cfg.getMinRange());
                    start = Integer.parseInt(cfg.getMinRange());
                }
                builder.append(random.nextInt(range) + start).append(split);
            } else if (meta.getColumnType().toString().equals(Const.META_TYPE_BIGINT)) {
                int range = 10000;
                int start = 0;
                if (cfg != null && cfg.getMaxRange() != null && cfg.getMinRange() != null) {
                    range = (Integer.parseInt(cfg.getMaxRange()) - Integer.parseInt(cfg.getMinRange()));
                    start = Integer.parseInt(cfg.getMinRange());
                }
                builder.append(random.nextInt(range) + start).append(split);
            } else if (meta.getColumnType().toString().equals(Const.META_TYPE_DOUBLE) || meta.getColumnType().toString().equals(Const.META_TYPE_NUMERIC)) {
                double range = 10000.0;
                double start = 0.0;
                if (cfg != null && cfg.getMaxRange() != null && cfg.getMinRange() != null) {
                    range = (Double.parseDouble(cfg.getMaxRange()) - Double.parseDouble(cfg.getMinRange()));
                    start = Double.parseDouble(cfg.minRange);
                }
                double d1 = random.nextDouble() * range + start;
                builder.append(numberFormat.format(d1)).append(split);
            } else if (meta.getColumnType().toString().equals(Const.META_TYPE_TIMESTAMP) || meta.getColumnType().toString().equals(Const.META_TYPE_DATE)) {
                int diffDay = 30;
                boolean genrandom = true;
                boolean genincrement = false;
                long baseTs = 0L;
                if (cfg != null) {
                    if (cfg.getDateOffset().startsWith("increment")) {
                        genrandom = false;
                        genincrement = true;
                        baseTs = Long.parseLong(cfg.getDateOffset().substring(10, cfg.dateOffset.length()));

                    } else {
                        diffDay = Integer.parseInt(cfg.getDateOffset());
                    }
                }
                if (genrandom) {
                    //随机到秒，后的random作为毫秒random
                    long ts = System.currentTimeMillis() - random.nextInt(diffDay * 3600 * 24) * 1000L + random.nextInt(1000);
                    builder.append(format.format(new Timestamp(ts))).append(split);
                } else if (genincrement) {
                    long ts = baseTs + pos * 1000L;
                    builder.append(format.format(new Timestamp(ts))).append(split);
                }
            } else if (meta.getColumnType().toString().equals(Const.META_TYPE_STRING)) {
                int range = 26;
                int length = 16;
                boolean needgenstr = true;
                if (cfg != null) {
                    if (cfg.getStrCfg() != null) {
                        String[] strCfg = cfg.getStrCfg().split("\\|");
                        for (String s : strCfg) {
                            if (s.startsWith("num:")) {
                                int crange = 10000;
                                int cstart = 0;
                                String[] rangeStr = s.substring(4).split("-");
                                if (rangeStr.length >= 2) {
                                    cstart = (!StringUtils.isEmpty(rangeStr[0])) ? Integer.parseInt(rangeStr[0]) : 0;
                                    int cend = (!StringUtils.isEmpty(rangeStr[1])) ? Integer.parseInt(rangeStr[1]) : 10000;
                                    crange = (cend - cstart);
                                } else {
                                    crange = Integer.parseInt(strCfg[1]);
                                }
                                builder.append(cstart + random.nextInt(crange)).append(split);
                                needgenstr = false;
                            } else if ("increment:".equals(s)) {
                                Integer cstart = 0;
                                if (!StringUtils.isEmpty(s.substring(10))) {
                                    cstart = Integer.valueOf(s.substring(10));
                                }
                                builder.append((cstart + rows)).append(split);
                                needgenstr = false;
                            } else if ("dict".equals(s) && dictMap.containsKey(meta.getColumnName())) {
                                String dictVal = dictMap.get(meta.getColumnName()).get(random.nextInt(dictMap.get(meta.getColumnName()).size()));
                                builder.append(dictVal).append(split);
                            } else {
                                builder.append(s);
                            }
                        }
                    }
                }
                if (needgenstr) {
                    for (int i = 0; i < length; i++) {
                        builder.append((char) (random.nextInt(range) + 65));
                    }
                    builder.append(split);
                }
            }
        }
    }

    @Data
    public static class MetaCfg {
        private String columnName;
        private String columnType;
        private Integer columnLength;
        private String minRange;
        private String maxRange;
        private String dateOffset;
        private String strCfg;
        private String genType;

        public MetaCfg(String columnName, String columnType, Integer columnLength,
                       String minRange, String maxRange, String dateOffset, String strCfg, String genType) {
            this.columnLength = columnLength;
            this.columnName = columnName;
            this.columnType = columnType;
            this.strCfg = strCfg;
            this.minRange = minRange;
            this.maxRange = maxRange;
            this.dateOffset = dateOffset;
            this.genType = genType;
        }
    }
}
