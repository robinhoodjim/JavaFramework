package com.robin.comm.db;

import com.robin.core.fileaccess.meta.DataCollectionMeta;

import java.util.Map;

@FunctionalInterface
public interface ITransOperation {
    void doOperation(Map<String,Object> input, Map<String,String> mappingMap,DataCollectionMeta sourceMeta, DataCollectionMeta targetMeta);
}
