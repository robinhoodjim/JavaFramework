package com.robin.comm.db;


import com.robin.core.base.dao.SimpleJdbcDao;
import com.robin.core.fileaccess.meta.DataCollectionMeta;
import com.robin.core.query.extractor.ResultSetOperationExtractor;

import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;

public class CommDataSyncUtils {
    private CommDataSyncUtils(){

    }

    public static void syncWithQuery(Connection source,Connection target,String sourceTable,String targetTable,String querySql,Object[] queryObjs){
        try {
            SimpleJdbcDao.executeOperationWithQuery(source, querySql, queryObjs, true, new ResultSetOperationExtractor() {
                private DataCollectionMeta sourceMeta;
                @Override
                public boolean executeAdditionalOperation(Map<String, Object> map, ResultSetMetaData rsmd) throws SQLException {

                    return false;
                }
            });
        }catch (SQLException ex){

        }
    }

}
