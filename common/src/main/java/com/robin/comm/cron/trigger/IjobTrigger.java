package com.robin.comm.cron.trigger;

public interface IjobTrigger {
    void init();
    boolean parseJobs();
    int getDefaultPriority(String runCycle);

}
