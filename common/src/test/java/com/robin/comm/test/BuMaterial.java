package com.robin.comm.test;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 业务材料表
 * </p>
 *
 * @author sqq
 * @since 2020-03-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

public class BuMaterial implements Serializable {



    private Long id;
    /**
     * 业务事项主键id
     */
    private Long busiId;
    /**
     * 材料名称
     */
    private String name;
    /**
     * 材料序号
     */
    private String number;
    /**
     * 提交方式 (现场,网上,网上现场均可)
     */
    private String submitType;
    /**
     * 是否业务办理所需 true为是，false为否
     */
    private Boolean isMust;
    /**
     * 是否为电子材料 true为是，false为否
     */
    private Boolean isElecmaterial;
    /**
     * 业务材料提供单位
     */
    private Long provideBy;
    /**
     * 是否办结产生资料 true为是，false为否
     */
    private Boolean isSettle;
    /**
     * 来源渠道
     */
    private String sourceChannel;
    /**
     * 租户id
     */
    private Long tenantId;
    /**
     * 创建者
     */
    private Long creator;
    /**
     * 修改者
     */
    private Long modifier;
    /**
     * 修改时间
     */
    private Timestamp modifyTm;
    /**
     * 创建时间
     */
    private Timestamp createTm;
    private Short status;

}
