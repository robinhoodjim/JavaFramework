package com.robin.dataming;


import com.robin.core.base.util.Const;
import com.robin.core.base.util.ResourceConst;
import com.robin.core.fileaccess.iterator.AbstractFileIterator;
import com.robin.core.fileaccess.iterator.TextFileIteratorFactory;
import com.robin.core.fileaccess.meta.DataCollectionMeta;
import com.robin.core.fileaccess.meta.DataSetColumnMeta;
import com.robin.dataming.weka.algorithm.LinearRegressionModeler;
import com.robin.dataming.weka.utils.WekaUtils;
import org.junit.Test;
import weka.classifiers.Classifier;
import weka.core.Instances;

import java.util.Arrays;
import java.util.HashMap;


public class LinearRegressionTest {
    @Test
    public void testIris() throws Exception{
        DataCollectionMeta meta=new DataCollectionMeta();
        meta.setResType(ResourceConst.ResourceType.TYPE_LOCALFILE.getValue());
        meta.setSourceType(ResourceConst.IngestType.TYPE_LOCAL.getValue());
        meta.setFileFormat(Const.FILESUFFIX_CSV);
        meta.setPath("file:///e:/iris.csv");
        //"erwidth","banlength","banwidth","class"
        meta.addColumnMeta("erlength", Const.META_TYPE_DOUBLE,null);
        meta.addColumnMeta("erwidth", Const.META_TYPE_DOUBLE,null);
        meta.addColumnMeta("banlength", Const.META_TYPE_DOUBLE,null);
        meta.addColumnMeta("banwidth", Const.META_TYPE_DOUBLE,null);
        DataSetColumnMeta columnMeta=meta.createColumnMeta("class",Const.META_TYPE_STRING,null);
        columnMeta.setNominalValues(Arrays.asList(new String[]{"Iris-setosa", "Iris-versicolor","Iris-virginica"}));
        meta.addColumnMeta(columnMeta);
        AbstractFileIterator iterator= TextFileIteratorFactory.getProcessIteratorByType(meta);
        Instances instances= WekaUtils.getInstancesByResource(meta,iterator,4);
        LinearRegressionModeler modeler=new LinearRegressionModeler();
        Classifier classifier= modeler.train(4,new HashMap<>(),instances,instances);

    }
}
