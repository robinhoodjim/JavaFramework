package com.robin.core.web.struts2;

import com.robin.core.base.service.BaseAnnotationJdbcService;
import com.robin.core.base.spring.SpringContextHolder;
import com.robin.core.base.util.Const;
import com.robin.core.query.util.PageQuery;
import com.robin.core.web.util.Session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public abstract class Struts2CrudAction extends Struts2BaseAction{
	private BaseAnnotationJdbcService baseService;
	public void setService(BaseAnnotationJdbcService service){
		this.baseService=service;
	}
	public void setService(String beanName){
		this.baseService=(BaseAnnotationJdbcService) SpringContextHolder.getBean(beanName);
	}




	/**
	 * 列表查询方法
	 * @param request
	 * @param response
	 * @return
	 */
	protected abstract String doList(HttpServletRequest request,HttpServletResponse response);
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	protected abstract String doInitPage(HttpServletRequest request,HttpServletResponse response);
	
	protected abstract String doAdd(HttpServletRequest request,HttpServletResponse response);
	
	protected abstract String doSave(HttpServletRequest request,HttpServletResponse response);
	
	protected abstract String doUpdate(HttpServletRequest request,HttpServletResponse response);
	
	protected abstract String doView(HttpServletRequest request,HttpServletResponse response);
	
	protected abstract String doDelete(HttpServletRequest request,HttpServletResponse response);

	public void doAddOper(){

	}
	
	public Session getCurrentUser(HttpServletRequest request) {
		return (Session) request.getSession().getAttribute(Const.SESSION);
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public PageQuery getQuery() {
		if (query==null) {
			query=new PageQuery();
		}
		return query;
	}
	public void setQuery(PageQuery query) {
		this.query = query;
	}
	public Map<String, String> getRecord() {
		if(record==null)
			record=new HashMap<String, String>();
		return record;
	}
	public void setRecord(Map<String, String> record) {
		this.record = record;
	}
}
