package com.robin.core.web.struts2;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.robin.core.base.spring.SpringContextHolder;
import com.robin.core.base.util.Const;
import com.robin.core.convert.util.ConvertUtil;
import com.robin.core.query.util.PageQuery;
import com.robin.core.web.codeset.Code;

import com.robin.core.web.codeset.CodeSetService;
import com.robin.core.web.util.Session;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public abstract class Struts2BaseAction extends ActionSupport{
	protected final Log						log				= LogFactory.getLog(getClass());
	protected String							action;

	protected String							status;

	protected String							errorMessage="";
	protected Map<String, String>	record;

	protected PageQuery query;

	protected static final String			LIST				= "LIST";

	protected static final String			ADD				= "ADD";

	protected static final String			SAVE				= "SAVE";

	protected static final String			EDIT				= "EDIT";

	protected static final String			UPDATE			= "UPDATE";

	protected static final String			DELETE			= "DELETE";

	protected static final String			VIEW				= "VIEW";

	protected static final String			SEARCH			= "SEARCH";

	protected static final String			MESSAGE			= "message";

	protected static final String			ERROR				= "error";
	
	protected static final String 			INITPAGE		= "INITPAGE";

	protected static final int				ERROR_MSG_SIZE	= 30;

	protected static final String			RETURN_NORMAL	= "0";
	
	protected static final String			RETURN_NOREFRESH	= "-1";

	protected static final String			NOTLOGIN			= "notLogin";

	protected static final String			NOPRIVILEGE		= "noPrivilege";

	protected static final String			HAVEPRIVILEGE	= "havePrivilege";

	
	public String execute() throws Exception {
//		if (log.isDebugEnabled()) log.debug("enter execute...");
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response=ServletActionContext.getResponse();
		String forward="";
		Map map=ActionContext.getContext().getSession();
		Session u1=(Session) map.get(Const.SESSION);
		Session currentUser = (Session) request.getSession().getAttribute(Const.SESSION);
		String islogon = checkLogin(request, currentUser);
		
		if (islogon.equals(NOTLOGIN)) return "relogin";
		else if (islogon.equals(NOPRIVILEGE)) return NOPRIVILEGE;
		else {
			
			response.setHeader("Content-Type", "text/html; charset=UTF-8");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			forward = doExecute(request,response);
			return forward;
			
		}
		//forward = doExecute(request,response);
		//return forward;
	}
	public abstract String doExecute(HttpServletRequest request, HttpServletResponse response) throws Exception;
	public Object getBean(String name) {
		return SpringContextHolder.getBean(name);
	}

	protected String getErrorMsg(Exception e, int size) {
		String errorMsg = e.getMessage();
		if (errorMsg != null && errorMsg.length() > size) {
			errorMsg = errorMsg.substring(0, size) + "...";
		}
		return errorMsg;
	}

	/**
	 * Check Login need to Override
	 * @param request
	 * @return
	 */
	public String checkLogin(HttpServletRequest request, Session currentUser) {
		if (currentUser == null) return NOTLOGIN;
		else {
			String uriStr = request.getRequestURI();
			String action = request.getParameter("action");
			String contextPath = request.getContextPath();
			String urlPath = uriStr.replaceAll(contextPath + "/", "");

			if (action != null) urlPath += "?action=" + action;
			else urlPath += "?action=list";

			 return HAVEPRIVILEGE;
		}
	}
	/**
	 * 在ActionForm中增加一个提示信息
	 *
	 * @param info
	 *           提示信息
	 */
	protected void addMessage( String info) {
		String msg = getErrorMessage();
		if (!msg.equals("")) msg = msg + "<br>";
		setErrorMessage(msg + info);
	}
	protected String findCodeName(String codeNo, String value) {

		CodeSetService util= (CodeSetService) SpringContextHolder.getBean(CodeSetService.class);
		List<Code> list = getCodeList(util.getCacheCode(codeNo));
		if (value == null) return "";
		if (list == null) return "";
		for (int i = 0; i < list.size(); i++) {
			Code code = list.get(i);
			if (value.equals(code.getValue())) return code.getCodeName();
		}
		return "";
	}
	protected List<Code> getCodeList(Map<String,String> codeMap){
		List<Code> retlist=new ArrayList<>();
		Iterator<Map.Entry<String,String>> iter= codeMap.entrySet().iterator();
		while(iter.hasNext()){
			Map.Entry<String,String> entry=iter.next();
			retlist.add(new Code(entry.getValue(),entry.getKey()));
		}
		return retlist;
	}

	protected void setCode(String codeSetNos) {
		String[] codes;
		if (codeSetNos == null) return;
		if (codeSetNos.indexOf(";") > 0) codes = codeSetNos.split(";");
		else if (codeSetNos.indexOf(",") > 0) codes = codeSetNos.split(",");
		else {
			codes = new String[1];
			codes[0] = codeSetNos;
		}
		CodeSetService util= (CodeSetService) SpringContextHolder.getBean(CodeSetService.class);
		for (int i = 0; i < codes.length; i++)
		{
			util.getCacheCode(codes[i]);
		}
	}
	protected void filterListByCodeSet(String columnName,String codeNo){
		if(!query.getRecordSet().isEmpty()){
			List<Map<String, Object>> list=query.getRecordSet();
			for (Map<String,Object> map:list) {
				String name=findCodeName(codeNo, map.get(columnName).toString());
				if(name!=null && !"".equals(name)){
					map.put(columnName, name);
				}
			}
		}
	}
	protected void filterListByCodeSet(List<Map<String, String>> list,String columnName,String codeNo){
		if(!list.isEmpty()){
			for (Map<String,String> map:list) {
				String name=findCodeName(codeNo, map.get(columnName));
				if(name!=null && !"".equals(name)){
					map.put(columnName, name);
				}
			}
		}
	}
	protected List<Map<String, String>> convertObjToMapList(List<?> orglist) throws Exception{
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		if (orglist != null && !orglist.isEmpty()) {
			for (Object object : orglist) {
				Map<String, String> map = new HashMap<String, String>();
				ConvertUtil.objectToMap(map, object);
				list.add(map);
			}
		}
		return list;
	}


	protected List<Code> findCodeSetArr(String codeSetNo){
		CodeSetService util= (CodeSetService) SpringContextHolder.getBean(CodeSetService.class);
		return getCodeList(util.getCacheCode(codeSetNo));
	}

	protected void setCode(String codeSetNo, List codes, String label, String value) {
		CodeSetService util= (CodeSetService) SpringContextHolder.getBean(CodeSetService.class);
		util.setCode(codeSetNo,codes,label,value);
	}

	protected String returnForward(String returnStatus) {
		HttpServletRequest request = ServletActionContext.getRequest();
		request.setAttribute("returnStatus", returnStatus);
		return MESSAGE;
	}

	/**
	 * 设置查询数据库需要的分页属性
	 *
	 * @param query
	 *           分页查询对象
	 */
	protected void setPage(PageQuery query) {
		int currentPage;
		int pageCount;
		int recordCount =query.getRecordCount();
		if (recordCount == 0) {
			query.setPageNumber(0);
			query.setPageCount(0);
		}
		else {
			int pageSize = new Integer(query.getPageSize()).intValue();
			if (pageSize != 0) {
				pageCount = recordCount / pageSize;
				if (recordCount % pageSize > 0) pageCount++;
			}
			else pageCount = 1;
			query.setPageCount(pageCount);
			currentPage = new Integer(query.getPageNumber()).intValue();
			if (currentPage <= 0) currentPage = 1;
			if (currentPage > pageCount) currentPage = pageCount;
			query.setPageNumber(currentPage);
		}
	}

	public Session getCurrentUser(HttpServletRequest request) {
		return (Session) request.getSession().getAttribute(Const.SESSION);
	}

	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public PageQuery getQuery() {
		if (query==null) {
			query=new PageQuery();
		}
		return query;
	}
	public void setQuery(PageQuery query) {
		this.query = query;
	}
	public Map<String, String> getRecord() {
		if(record==null)
			record=new HashMap<String, String>();
		return record;
	}
	public void setRecord(Map<String, String> record) {
		this.record = record;
	}
}
