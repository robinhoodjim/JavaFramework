package com.robin.tracer.spring.advisor;

import com.robin.tracer.plugin.datasource.DataSourceInterceptor;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;

/**
 * <p>Project:  frame</p>
 *
 * <p>Description: DataSourceAdvisor </p>
 *
 * <p>Copyright: Copyright (c) 2021 modified at 2021-07-06</p>
 *
 * <p>Company: seaboxdata</p>
 *
 * @author luoming
 * @version 1.0
 */
public class DataSourceAdvisor extends AbstractPointcutAdvisor  {
    private Advice advice;
    private Pointcut pointcut;

    public DataSourceAdvisor(Pointcut pointcut, DataSourceInterceptor interceptor){
        this.advice=interceptor;
        this.pointcut=pointcut;
    }

    /*public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        if (this.advice instanceof BeanFactoryAware) {
            ((BeanFactoryAware)this.advice).setBeanFactory(beanFactory);
        }

    }*/

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }

    @Override
    public Advice getAdvice() {
        return this.advice;
    }
}
