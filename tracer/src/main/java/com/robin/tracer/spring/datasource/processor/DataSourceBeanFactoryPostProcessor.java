package com.robin.tracer.spring.datasource.processor;

import brave.Tracing;
import com.robin.tracer.plugin.datasource.ProxyDataSource;
import com.robin.tracer.plugin.datasource.util.DataSourceUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.*;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>DataSource Bean Factory，代理数据库操作，记录tracer日志,参考Sofatracer的实现</p>
 *
 * @author robinjim
 * @version 1.0
 */
public class DataSourceBeanFactoryPostProcessor implements BeanFactoryPostProcessor, EnvironmentAware, ApplicationContextAware, PriorityOrdered {
    private Environment environment;
    public static final String ENHANCER_DATASOURCE_PREFIX="_eh_";
    public static final String APPNAME_KEY="spring.application.name";
    private ApplicationContext context;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        for(String beanName:getBeanNames(beanFactory, DataSource.class)){
            if(beanName.startsWith(ENHANCER_DATASOURCE_PREFIX)){
                continue;
            }
            BeanDefinition dataSource=getBeanDefinition(beanName,beanFactory);
            if (DataSourceUtils.isDruidDataSource(dataSource.getBeanClassName())) {
                createDataSourceProxy(beanFactory, beanName, dataSource,
                        DataSourceUtils.getDruidJdbcUrlKey());
            } else if (DataSourceUtils.isC3p0DataSource(dataSource.getBeanClassName())) {
                createDataSourceProxy(beanFactory, beanName, dataSource,
                        DataSourceUtils.getC3p0JdbcUrlKey());
            } else if (DataSourceUtils.isDbcpDataSource(dataSource.getBeanClassName())) {
                createDataSourceProxy(beanFactory, beanName, dataSource,
                        DataSourceUtils.getDbcpJdbcUrlKey());
            } else if (DataSourceUtils.isTomcatDataSource(dataSource.getBeanClassName())) {
                createDataSourceProxy(beanFactory, beanName, dataSource,
                        DataSourceUtils.getTomcatJdbcUrlKey());
            } else if (DataSourceUtils.isHikariDataSource(dataSource.getBeanClassName())) {
                createDataSourceProxy(beanFactory, beanName, dataSource,
                        DataSourceUtils.getHikariJdbcUrlKey());
            }
        }
    }

    private Iterable<String> getBeanNames(ListableBeanFactory beanFactory, Class clazzType) {
        Set<String> names = new HashSet<>();
        names.addAll(Arrays.asList(BeanFactoryUtils.beanNamesForTypeIncludingAncestors(beanFactory,
                clazzType, true, false)));
        return names;
    }
    private BeanDefinition getBeanDefinition(String beanName,
                                             ConfigurableListableBeanFactory beanFactory) {
        try {
            return beanFactory.getBeanDefinition(beanName);
        } catch (NoSuchBeanDefinitionException ex) {
            BeanFactory parentBeanFactory = beanFactory.getParentBeanFactory();
            if (parentBeanFactory instanceof ConfigurableListableBeanFactory) {
                return getBeanDefinition(beanName,
                        (ConfigurableListableBeanFactory) parentBeanFactory);
            }
            throw ex;
        }
    }
    private void createDataSourceProxy(ConfigurableListableBeanFactory beanFactory,
                                       String beanName, BeanDefinition originDataSource,
                                       String jdbcUrl) {
        // re-register origin datasource bean
        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) beanFactory;
        beanDefinitionRegistry.removeBeanDefinition(beanName);
        boolean isPrimary = originDataSource.isPrimary();
        originDataSource.setPrimary(false);
        beanDefinitionRegistry.registerBeanDefinition(transformDatasourceBeanName(beanName),
                originDataSource);
        // register proxied datasource
        RootBeanDefinition proxiedBeanDefinition = new RootBeanDefinition(ProxyDataSource.class);
        proxiedBeanDefinition.setRole(BeanDefinition.ROLE_APPLICATION);
        proxiedBeanDefinition.setPrimary(isPrimary);
        proxiedBeanDefinition.setInitMethodName("init");
        proxiedBeanDefinition.setDependsOn(transformDatasourceBeanName(beanName));
        MutablePropertyValues originValues = originDataSource.getPropertyValues();
        MutablePropertyValues values = new MutablePropertyValues();
        String appName = environment.getProperty(APPNAME_KEY);

        values.add("appName", appName);
        values.add("delegate", new RuntimeBeanReference(transformDatasourceBeanName(beanName)));
        values.add("dbType",
                DataSourceUtils.resolveDbTypeFromUrl(unwrapPropertyValue(originValues.get(jdbcUrl))));
        values.add("database",
                DataSourceUtils.resolveDatabaseFromUrl(unwrapPropertyValue(originValues.get(jdbcUrl))));
        values.add("tracing",context.getBean(Tracing.class));
        values.add("traceEnable",environment.containsProperty("zhcx.datasource.tracing") && "true".equalsIgnoreCase(environment.getProperty("maven.compiler.source")));
        proxiedBeanDefinition.setPropertyValues(values);
        beanDefinitionRegistry.registerBeanDefinition(beanName, proxiedBeanDefinition);
    }
    public static String transformDatasourceBeanName(String originName) {
        return ENHANCER_DATASOURCE_PREFIX + originName;
    }
    protected String unwrapPropertyValue(Object propertyValue) {
        if (propertyValue instanceof TypedStringValue) {
            return ((TypedStringValue) propertyValue).getValue();
        } else if (propertyValue instanceof String) {
            return (String) propertyValue;
        }
        throw new IllegalArgumentException(
                "The property value of jdbcUrl must be the type of String or TypedStringValue");
    }
    @Override
    public void setEnvironment(Environment environment) {
        this.environment=environment;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context=applicationContext;
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}
