package com.robin.tracer.spring.datasource.processor;

import brave.Tracing;
import com.robin.tracer.plugin.datasource.ProxyDataSource;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

/**
 * <p>DataSource Bean Factory，代理数据库操作，记录tracer日志,参考Sofatracer的实现</p>
 *
 * @author robinjim
 * @version 1.0
 */

public class DataSourceBeanPostProcessor implements BeanPostProcessor, EnvironmentAware, ApplicationContextAware, PriorityOrdered {
    private Environment environment;
    private ApplicationContext applicationContext;

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String s) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if(beanName.startsWith(DataSourceBeanFactoryPostProcessor.ENHANCER_DATASOURCE_PREFIX) || bean instanceof ProxyDataSource || !(bean instanceof DataSource)){
            return bean;
        }
        return new ProxyDataSource((DataSource)bean,environment,applicationContext.getBean(Tracing.class));
    }

    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}
