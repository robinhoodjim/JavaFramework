package com.robin.tracer.plugin.datasource;

import brave.Tracing;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.sql.Connection;


public class DataSourceInterceptor implements MethodInterceptor, ApplicationContextAware {
    private ApplicationContext applicationContext;
    public DataSourceInterceptor(){

    }
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        Connection connection=(Connection) methodInvocation.proceed();

        return new ProxyConnection(connection,applicationContext.getBean(Environment.class),applicationContext.getBean(Tracing.class));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }
}
