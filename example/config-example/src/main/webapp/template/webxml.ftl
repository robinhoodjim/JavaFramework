<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://java.sun.com/xml/ns/j2ee"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="2.4"
	xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee   http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd">

	 
    <!-- Context Configuration locations for Spring XML files -->
    <#if (project.useMvc=='0')>
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>/WEB-INF/config/spring/applicationContext-*.xml</param-value>
    </context-param>
    </#if>
    <context-param>
        <param-name>queryConfig</param-name>
        <param-value>/WEB-INF/config/queryConfig/query*.xml</param-value>
    </context-param>
    <filter>
        <filter-name>encodingFilter</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>UTF-8</param-value>
        </init-param>
        <init-param>
            <param-name>forceEncoding</param-name>
            <param-value>true</param-value>
        </init-param>
    </filter>
	<#if (project.presistType=='1')>
    <filter>
        <filter-name>hibernateFilter</filter-name>
        <filter-class>org.springframework.orm.hibernate3.support.OpenSessionInViewFilter</filter-class>
    </filter>
     <filter-mapping>
        <filter-name>hibernateFilter</filter-name>
        <url-pattern>*.action</url-pattern>
    </filter-mapping>
	</#if>
    <#if (project.useMvc=='0')>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>*.action</url-pattern>
    </filter-mapping>
    </#if>
    <filter-mapping>
        <filter-name>encodingFilter</filter-name>
        <url-pattern>*.jsp</url-pattern>
    </filter-mapping>
 	
   
  	<#if (project.useMvc=='0')>
	 <filter>
		<filter-name>struts2</filter-name>
		<filter-class>
			cn.com.talkweb.core.base.web.filter.Struts2Filter
		</filter-class>
		<init-param>
          <param-name>configLocation</param-name>
          <param-value>/WEB-INF/config/struts2</param-value>
         </init-param>
	</filter>
	<filter-mapping>
		<filter-name>struts2</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
	<#else>
	<servlet>
		<servlet-name>${project.projCode}</servlet-name>
		<servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
		<init-param> 
          <param-name>contextConfigLocation</param-name> 
          <param-value>/WEB-INF/config/spring/applicationContext-*.xml</param-value> 
        </init-param> 
	</servlet>
	</#if>
    <listener>
        <listener-class>org.springframework.web.util.IntrospectorCleanupListener</listener-class>
    </listener>

	<listener>
        <listener-class>cn.com.talkweb.core.base.web.listener.WebContextListener</listener-class>
    </listener>
   
    <session-config>
        <session-timeout>30</session-timeout>
    </session-config>

    <welcome-file-list>
        <welcome-file>index.html</welcome-file> 
        <welcome-file>index.jsp</welcome-file>        
        <welcome-file>login.jsp</welcome-file>
    </welcome-file-list>
    
</web-app>
