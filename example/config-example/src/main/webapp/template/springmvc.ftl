package ${class.webPackage};

import com.robin.core.base.exception.ServiceException;
import com.robin.core.base.model.BaseObject;
import com.robin.core.base.util.Const;
import com.robin.core.collection.util.CollectionBaseConvert;
import com.robin.core.query.util.PageQuery;
import com.robin.core.web.controller.AbstractCrudController;
import ${class.servicePackage}.${class.javaClass}Service;
import ${class.modelPackage}.${class.javaClass};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Title:${project.projName}</p>
 *
 * <p>Description:${project.description}</p>
 *
 * <p>Usage:${class.name} Contorller</p>
 *
 * <p>Company: ${project.company}</p>
 *
 * <p>Copyright: Copyright (c) ${CurYear}</p>
 *
 * <p>LastUpdate: ${LastUpdate}  By:${project.author} </p>
 **/
@Controller
@RequestMapping("${class.webPath}")
public class ${class.upperName}CrudController extends BaseCrudController<${class.upperName}, ${class.upperName}Service> {
<#if (project.presistType!='1')>
	@Autowired
	private ${class.upperName}Service ${class.springName}Service;
</#if>
    @RequestMapping("/show")
    public String show(HttpServletRequest request, HttpServletResponse response){
        return "${class.webPath}";
    }
    @RequestMapping("/list")
    @ResponseBody
    public Map<String,Object> list(HttpServletRequest request, HttpServletResponse response) {
        PageQuery query = wrapPageQuery(request);
        if (query == null)
            query = new PageQuery();
        query.setSelectParamId("GET_TEST");  //TODO modify here
        query.getParameters().put("queryCondition", wrapQuery(request));
        doQuery(request,response,query);
        wrapStatusBar(query);
        return wrapDhtmlxGridOutput(query);
    }
    @RequestMapping("/edit")
    @ResponseBody
    public Map<String,Object> query(HttpServletRequest request,
                             HttpServletResponse response){
        String id=request.getParameter("id");
        return doEdit(request,response,Long.valueOf(id));
    }
    @RequestMapping("/save")
    @ResponseBody
    public Map<String,Object> save(HttpServletRequest request,
                           HttpServletResponse response){
        return doAdd(request,response);
    }
    @RequestMapping("/update")
    @ResponseBody
    public Map<String, Object> update(HttpServletRequest request,
                                          HttpServletResponse response){
        Long id=Long.valueOf(request.getParameter("id"));
        return doUpdate(request,response,id);
    }
    @RequestMapping("/delete")
    @ResponseBody
    public Map<String,Object> delete(HttpServletRequest request,
                                         HttpServletResponse response) {
        Map<String, String> retmap = new HashMap<String, String>();
        Long[] ids=wrapPrimaryKeys(request.getParameter("ids"));
        return doDelete(request,response,ids);
    }

    public String wrapQuery(HttpServletRequest request){
        StringBuilder builder=new StringBuilder();
		//TODO add Query Condition Here
        if( request.getParameter("userName")!=null && !"".equals(request.getParameter("userName"))){
            builder.append(" and user_account like '%"+request.getParameter("userName")+"%'");
        }
        return builder.toString();
    }
}
