<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/config/tag/tw-tag.tld" prefix="tw" %>

<%
	String path = request.getContextPath();
	String CONTEXT_PATH = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path+"/";
	String msg = (String)request.getAttribute("msg");
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
	<title></title>
	<script src="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxLayout/codebase/dhtmlxcommon.js"></script>
    <script src="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxLayout/codebase/dhtmlxcontainer.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxWindows/codebase/dhtmlxwindows.css"/>
	<link rel="stylesheet" type="text/css" href="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxWindows/codebase/skins/dhtmlxwindows_dhx_skyblue.css"/>
	<script src="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxWindows/codebase/dhtmlxwindows.js"></script>
	<script src="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxForm/codebase/dhtmlxform.js"></script>
	<link rel="stylesheet" type="text/css" href="<%=CONTEXT_PATH%>css/main.css" />
	
	<script type="text/javascript" src="<%=CONTEXT_PATH%>js/jquery.js"></script>
	<script type="text/javascript" src="<%=CONTEXT_PATH%>js/jsLib.js"></script>
  <script language="javascript" src="<%= CONTEXT_PATH %>js/date_validate.js"></script>

  <script language="javascript" src="<%= CONTEXT_PATH %>js/ctrl_util.js"></script>
	 <script language="javascript" src="<%= CONTEXT_PATH %>js/datapicker/WdatePicker.js"></script>
	<script type="text/javascript">
      var contextpath = "<%=CONTEXT_PATH%>";	
      var fulllink = contextpath + "${class.webPath}.action";		
	  var filltemplate;
	  var defaultportarr;
	function goCancel(){		
		parent.closedialog();
	}
	function goSubmit(){
		//checkString参数：对象名、标题、长度、是否检测空值
		<#list fieldList as field>
			<#if (field.isNull=='0')>
			if(!checkIsNull(getElement('record["${field.code}"]'),"${field.name}")) return;
			</#if>
		</#list>

    	document.forms[0].submit();
	}
	
	
	</script>
<style type="text/css">
   html, body {width:100%; height:100%; margin:0px; padding:0px; overflow:hidden;padding-right:2px;}
</style>
</head>

<body onload="init()">
<script type="text/javascript">

var windows; 
function closeWindow(){	
	windows.window("win").close();
};

function openAlert(message){
	windows = new dhtmlXWindows();
	windows.enableAutoViewport(true);
	windows.setImagePath("<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlxWindows/codebase/imgs/");
	windows.setSkin("dhx_skyblue");
	var win = windows.createWindow("win",0,0,250,125);
	win.denyPark();
	win.denyMove();
	win.denyResize();
	win.button("minmax1").hide();
	win.button("park").hide();
	win.setText("确认");
	win.setModal(true);
	win.center();
	var div = "<div ><p align='center'>"+message+"</P><p align='center'><input type=button onclick='closeAlert()' value='确定'/></p></div>";
	win.attachHTMLString(div);
}

function closeAlert(){
	windows.window("win").close();
}

var msg = "<%= msg==null?"":msg%>";
if (msg!="")openAlert(msg);

</script>
<div id="formDiv" class="formDiv">
  <div id="my_form"></div>

  <s:form action="${class.webPath}" styleId="form1">
  <s:hidden name="action" />
  		<#list fields as field>
  		<#if (field.isPrimary=='1')>
  		<s:hidden name="record['${field.code}']" />
  		</#if>
  		</#list>
		<table id="tblForm" cellpadding="0" cellspacing="0" width="100%" class="formTable">
			<#list fieldList as field>
			<#assign index=field_index />
			<#if (index==0 || (index+1) %2!=0)>
			<tr>
			</#if>
			  <td width="15%"><font color="red">*</font>${field.name}：</td>
					<td width="35%">
						<#if (field.displayType=='1')>
					   <s:textfield name="record['${field.code}']" />
					   <#elseif (field.displayType=='2')>
					   <s:select name="record['${field.code}']" />
					   <#elseif (field.displayType=='3')>
					   <s:textfield name="record['${field.code}from']" styleClass="Wdate" />-
					   <s:textfield name="record['${field.code}to']" styleClass="Wdate" />
					   <#else>
					   <s:checkbox name="record['${field.code}']" />
					   </#if>
					</td>
			<#if (index!=0 && (index+1)%2==0)>
			</tr>
			</#if>
			<#if !field_has_next>
			<#if (index+1)%2!=0>
			<td></td><td></td>
			</tr>
			</#if>
			</#if>
			</#list>
	 </table>
  </s:form>
  </div>
  <div id="btnDiv" class="btnDiv">
  			
	       <input type="button" name="btnAdd" onClick="goSubmit()" value="保存" />
				    &nbsp;
			   <input type="button" name="btnCancel" onClick="goCancel()" value="取消" />
				    &nbsp;
			      <input type="button" name="btnReset" onClick="goReset()" value="重置" />
</div>	  

<%@include file="/common/dialog1.jsp" %>
<%@include file="/common/resize.jsp" %>
</body>
</html>
