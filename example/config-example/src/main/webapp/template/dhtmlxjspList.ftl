<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ taglib uri="/WEB-INF/config/tag/tw-tag.tld" prefix="tw"%>
<%
	String path = request.getContextPath();
	String CONTEXT_PATH = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	String msg = (String) request.getAttribute("msg");
	if (msg == null)
		msg = "";
%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<link rel="stylesheet" type="text/css" href="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlx/dhtmlx.css" />
		<script src="<%=CONTEXT_PATH%>dhtmlxSuite/dhtmlx/dhtmlx.js"></script>
		<link rel="stylesheet" type="text/css" href="<%=CONTEXT_PATH%>css/main.css" />
		<script src="<%=CONTEXT_PATH%>js/ctrl_util.js"></script>
		<script src="<%=CONTEXT_PATH%>js/date_validate.js"></script>
		<script language="javascript" src="<%= CONTEXT_PATH %>js/datapicker/WdatePicker.js"></script>
		
		<script language="javascript"> 
      var contextpath = "<%=CONTEXT_PATH%>";
      var hyperlink = contextpath + "${class.webPath}.action";
      var fulllink = contextpath + "${class.webPath}.action";
      
		 function goAdd()  {
			openWindow("添加",fulllink+'?action=ADD',700,400);
		}
		function goModify() {
			
			<#list fields as field>
			<#if (field.isPrimary=='1')>
			var url_link=fulllink+"?action=EDIT&record['${field.code}']="+id;
			var id = findSelected("${field.code}","修改");
			if(id == "") return;
			</#if>
		    </#list>
			openWindow("修改",url_link,700,400);
		}
		
		
		function goDel()  {
			<#list fields as field>
			<#if (field.isPrimary=='1')>
	  		var id = findMultiSelected("${field.code}","删除");
	  		if(id == "") return;
	  		</#if>
		    </#list>
		    var res = confirm("是否真的要删除?");
	  		if(res == true) {
       			document.forms[0].action = fulllink + "?action=delete&ids=" + id;
       			document.forms[0].target = "_self";
       			document.forms[0].submit();
  		 	}
    	}
    	
    	function goView() {
    		<#list fields as field>
			<#if (field.isPrimary=='1')>
			var id = findSelected("${field.code}","查看");
				if(id == "") return;
		    var url_link=fulllink+'?action=VIEW&record["${field.code}"]='+id;
			</#if>
			</#list>
		
			openWindow("查看",url_link,700,400);
		}
    	
		function renew() {
			var order = getElement("query.order");                  order.value="";
			var desc =  getElement("query.orderDirection");         desc.value="";
			var pn =    getElement("query.pageNumber");             pn.value="1";
			var ps =    getElement("query.pageSize");               ps.value="10";
		
    		gosearch();
		}

		function goQuery() {
			var pn = getElement("query.pageNumber");             
			pn.value="1";
			gosearch();
		}

		function closedialog(ret) {
			dhxWins.window(winName).close();
      		editMode="";
			if(ret=='true') {
				gosearch();
			}	
		}
		

		
		function alertMsg(msgtxt,heigth){
      		dhtmlx.message({
    		title: '提示',
                    type: "alert-warning",
    		text: msgtxt,
    		callback: function() {};
   		 });
	   }		

		var msg = "<%=msg%>";
		if (msg != "")
			alert(msg);
			

	</script>

	</head>

	<body>
	<div class="special-padding">   
		<s:form action="${class.webPath}" method="post">
			
			<table cellpadding="0" cellspacing="0" class="formTable">
				<#list queryfieldList as field>
				<#assign index=field_index/>
				<#if (index==0 || (index+1) %2!=0)>
				<tr>
				</#if>
				 	<td width="20%">
						 ${field.name}：
 					</td>
 					<td width="30%">
 						<#if (field.displayType=='1')>
  						<s:textfield  name="query.parameters['${field.code}']" />
  						<#elseif (field.displayType=='2')>
  						<s:select  name="query.parameters['${field.code}']" />
  						<#else>
  						<s:checkbox name="query.parameters['${field.code}']" />
  						</#if>
 					</td>
 					<#if ((index+1)%2==0)>
					</tr>
					</#if>
					<#if (!field_has_next && (index+1)%2!=0)>
					<td></td><td></td>
					</tr>
					</#if>
					</#list>
				 	
					<tr class="btnTr">
						<td class="textC" colspan="4">
							<input type="button" name="btnQuery" onClick="goQuery()" value="查询" />
				      &nbsp;
			      			<input type="button" name="btnRefresh" onClick="renew()" value="重置" />
						</td>
					</tr>
				
				
			</table>
  			<div class="gap8">&nbsp;</div>  
			<table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
				<tr>
            		<td width="100%" valign="top">
						<table cellspacing="1" cellpadding="1" class="controlTable">
							<tr>
								<td valign="top" align="left">
									<tw:button styleClass="sbuBtnStyle" icon="addIcon" onClick="goAdd()">添加</tw:button>&nbsp;
									<tw:button styleClass="sbuBtnStyle" icon="subIcon" onClick="goModify()">修改</tw:button>&nbsp;
									<tw:button styleClass="sbuBtnStyle" icon="subIcon" onClick="goView()">查看</tw:button>&nbsp;
									<tw:button styleClass="sbuBtnStyle" icon="delIcon" onClick="goDel()">删除</tw:button>&nbsp;
									
								</td>
							</tr>
						</table>
						<s:hidden name="query.order" />
						<s:hidden name="query.orderDirection" />
						<s:hidden name="query.pageNumber" />
						<s:hidden name="query.recordCount" />
						<s:hidden name="query.pageCount" />
							
					</td>
				</tr>
				<tr>
				<tw:grid2 cellPadding="0" cellSpacing="0" width="100%" styleClass="listTable"
	    					   name="query" property="recordSet" parameters="query" rowEventHandle="false" fixRows="false">
	      						 <header style="" height="27"  />
	      						<#list fields as field>
	      						<#if (field.isPrimary=='1')>
    							<column width="5%" itemType="checkbox" property="${field.code}" align="center" selectAll="true"  headerAlign="center"/>
  								</#if>
  								</#list>
  								<#list displayFieldList as field>
  								<column width="20%" name="${field.name}" property="${field.code}" align="center" headerOnMouseOut="headerOut(this)" headerOnClick="query('${field.code}')" headerOnMouseOver="headerOver(this)"/>
  								</#list>
    							<rooter height="30" width="100%" showType="all" />      
				</tw:grid2>
				</tr>
			</table>
		</s:form>
		</div>
		<%@include file="/common/dialog1.jsp"%>
	</body>
</html>