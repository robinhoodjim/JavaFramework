package ${class.webPackage};

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import cn.com.talkweb.core.base.exception.ServiceException;
import cn.com.talkweb.core.base.exception.WebException;
import cn.com.talkweb.core.base.util.ConvertUtil;
import cn.com.talkweb.core.base.util.PageQuery;
import cn.com.talkweb.core.base.web.action.Struts2BaseAction;
import org.apache.struts2.convention.annotation.Action;  
import org.apache.struts2.convention.annotation.Namespace;  
import org.apache.struts2.convention.annotation.ParentPackage;  
import org.apache.struts2.convention.annotation.Result;  
import org.springframework.context.annotation.Scope;  
import org.springframework.stereotype.Controller;  
import org.springframework.beans.factory.annotation.Autowired;
import ${class.servicePackage}.${class.javaClass}Service;
import ${class.modelPackage}.${class.javaClass};


/**
 * <p>Title:${project.projName}</p>
 *
 * <p>Description:${project.description}</p>
 *
 * <p>Usage:${class.name} 实体Action</p>
 *
 * <p>Company: ${project.company}</p>
 *
 * <p>Copyright: Copyright (c) ${CurYear}</p>
 *
 * <p>LastUpdate: ${LastUpdate}  By:${project.author} </p>
 **/
 
@Controller("${class.springName}Action")  
@Scope("prototype")  
@ParentPackage("struts-default") 
@Namespace("${class.webPath}") 
@Action("${class.webPath}")  
@Results({
	@Result(name="LIST",location="${class.pagePath}/${class.javaClass}_list.jsp"),
	@Result(name="ADD",location="${class.pagePath}/${class.javaClass}_add.jsp"),
	@Result(name="EDIT",location="${class.pagePath}/${class.javaClass}_edit.jsp"),
	@Result(name="VIEW",location="${class.pagePath}/${class.javaClass}_view.jsp")
})
public class ${class.upperName}Action extends Struts2BaseAction {
	<#if (project.presistType!='1')>
	@Autowired
	private ${class.upperName}Service ${class.springName}Service;
	</#if>
	@Override
	public String doExecute(HttpServletRequest request, HttpServletResponse response) {
		String action = request.getParameter("action");
		if (action == null) action = LIST;
		if (log.isDebugEnabled()) log.debug("action:" + action);
		String forward;
		
		try {
			if (LIST.equalsIgnoreCase(action)) forward = search${class.upperName}(request, response); // 打开查询列表页面
			else if (ADD.equalsIgnoreCase(action)) forward = add${class.upperName}(request, response);// 打开增加页面
			else if (SAVE.equalsIgnoreCase(action)) forward = save${class.upperName}(request, response);// 保存增加数据
			else if (EDIT.equalsIgnoreCase(action)) forward = edit${class.upperName}(request, response);// 打开修改页面
			else if (UPDATE.equalsIgnoreCase(action)) forward = update${class.upperName}(request, response);// 保存修改数据
			else if (DELETE.equalsIgnoreCase(action)) forward = delete${class.upperName}(request, response);// 删除
			else if (VIEW.equalsIgnoreCase(action)) forward = view${class.upperName}(request, response);// 浏览
			else {
				request.setAttribute("err", new WebException("找不到该action方法：" + action));
				forward = ERROR;// 找不到合适的action
			}
		}
		catch (Exception e) {// 其他系统出错
			e.printStackTrace();
			request.setAttribute("err", e);
			forward = ERROR;
		}
		return forward;
	}

	
	public String add${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'add${class.upperName}' method");

		try {
			initForm(ADD);
			setAction(SAVE);
			return ADD;
		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("err", e);
			return ERROR;
		}
	}
	public String save${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'save${class.upperName}' method");
		String forward = null;

		try {
			${class.javaClass} model = new ${class.javaClass}();
			ConvertUtil.mapToObject(model, getRecord());
			<#if (project.presistType=='1')>
			${class.upperName}Service service = (${class.upperName}Service) getBean("${class.springName}Service");
			service.create${class.upperName}(model);
			<#else>
			${class.springName}Service.saveEnity(model);
			</#if>
			forward = returnForward(RETURN_NORMAL);
		}
		catch (ServiceException e) {
			addMessage(e.getMessage());
			initForm(ADD);
			forward = ADD;
		}
		catch (Exception e) {
			e.printStackTrace();
			addMessage("新增操作失败!");
			initForm(ADD);
			forward =ADD;
		}
		return forward;
	}
	
	

	public String view${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'view${class.upperName}' method");
		String id = request.getParameter("id");

		try {
			<#if (project.presistType=='1')>
			${class.upperName}Service ${class.javaClass}Service = (${class.upperName}Service) getBean("${class.springName}Service");
			${class.upperName} model = (${class.upperName}) ${class.javaClass}Service.find${class.upperName}(new ${class.pkType}(id));
			<#else>
				${class.upperName} model = ${class.springName}Service.getEntity(new ${class.pkType}(id));
			</#if>
			ConvertUtil.objectToMap(getRecord(), model);

			initForm(VIEW);
			return VIEW;
		}
		catch (ServiceException e) {
			request.setAttribute("errMsg", e.getMessage());
			return ERROR;
		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("err", e);
			return ERROR;
		}
	}

	public String edit${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'edit${class.upperName}' method");
		String id = request.getParameter("id");

		try {
			<#if (project.presistType=='1')>
			${class.upperName}Service ${class.springName}Service = (${class.upperName}Service) getBean("${class.springName}Service");
			${class.upperName} model = (${class.upperName}) ${class.springName}Service.find${class.upperName}(new ${class.pkType}(id));
			<#else>
			${class.upperName} model =${class.springName}Service.getEntity(new ${class.pkType}(id));
			</#if>
			ConvertUtil.objectToMap(getRecord(), model);
			
			initForm(EDIT);
			setAction(UPDATE);
			return EDIT;
		}
		catch (ServiceException e) {
			request.setAttribute("errMsg", e.getMessage());
			return ERROR;
		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("err", e);
			return ERROR;
		}
	}

	

	public String update${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'update${class.upperName}' method");

		String forward = null;

		try {
			${class.upperName} model = new ${class.upperName}();	
			ConvertUtil.mapToObject(model, getRecord());
			<#if (project.presistType=='1')>
			${class.upperName}Service ${class.springName}Service = (${class.upperName}Service) getBean("${class.springName}Service");
			${class.springName}Service.update${class.upperName}(model);
			<#else>
			${class.springName}Service.updateEntity(model);
			</#if>
			forward = returnForward(RETURN_NORMAL);
		}
		catch (ServiceException e) {
			addMessage(e.getMessage());
			initForm(EDIT);
			forward = EDIT;
		}
		catch (Exception e) {
			e.printStackTrace();
			addMessage("修改操作失败!");
			initForm(EDIT);
			forward = EDIT;
		}
		return forward;
	}

	public String delete${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'delete${class.upperName}' method");
		try {
			String del_ids = request.getParameter("ids");
			<#if (project.presistType=='1')>
			${class.upperName}Service ${class.springName}Service = (${class.upperName}Service) getBean("${class.springName}Service");
			if (del_ids != null && del_ids.trim().length() > 0) {
				${class.springName}Service.remove${class.upperName}s(parseId(del_ids.split(";")));
			}
			<#else>
			${class.springName}Service.deleteEntity(parseId(del_ids.split(";"));
			</#if>
		}
		catch (Exception e) {
			e.printStackTrace();
			addMessage("删除操作失败!");
		}
		return searchSysDept(request, response);
	}

	

	public String search${class.upperName}(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) log.debug("Entering 'search${class.upperName}' method");
		try {
			<#if (project.presistType=='1')>
			${class.upperName}Service ${class.springName}Service = (${class.upperName}Service) getBean("${class.springName}Service");
			</#if>
			PageQuery pageQuery = getQuery();
			//TODO: 修改以下语句,参数为配置在xml中的查询语句
			pageQuery.setSelectParamId("GET_SYSUSER_PAGE");
			String queryString = getQueryString(pageQuery);
			pageQuery.getParameters().put("queryString", queryString);
			if (pageQuery.getOrder() == null || "".equals(pageQuery.getOrder().trim())) {
				pageQuery.setOrder("id");
				pageQuery.setOrderDirection("desc");
			}
			<#if (project.presistType=='1')>
			${class.springName}Service.query${class.upperName}(pageQuery);
			<#else>
			${class.springName}Service.queryBySelectId(pageQuery);
			</#if>
			initForm(LIST);
			
			setPage(getQuery());
		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("err", e);
			return ERROR;
		}
		return LIST;
	}
	

	private java.io.Serializable[] parseId(String[] ids) throws Exception {
		if (ids == null || ids.length == 0) {
			throw new Exception("非法进入编辑页！");
		}
		java.io.Serializable id[] = null;
		try {
			id = new ${class.pkType}[ids.length];
			for (int i = 0; i < ids.length; i++) {
				id[i] = new ${class.pkType}(ids[i]);
			}
		}
		catch (Exception e) {
			id = ids;
		}
		return id;
	}

	private void initForm(String action) {
		/**  Add Filter form Code here  **/
		
	}

	private String getQueryString(PageQuery pageQuery) {
		StringBuffer buffer = new StringBuffer();
		/** Add Query Code here **/
		return buffer.toString();
	}

	
	
}

