<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:context="http://www.springframework.org/schema/context"  
	xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd">
	<bean id="propertyConfigurer" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
        <property name="locations">
            <list>
                <value>classpath:application.properties</value>
            </list>
        </property>
    </bean>
    
      
    <!-- JDBC DataSource -->
   <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource" destroy-method="close">
        <property name="driverClassName" value="\${jdbc.driver}"/>
      <property name="url" value="\${jdbc.url}"/>
        <property name="username" value="\${jdbc.username}"/>
        <property name="password" value="\${jdbc.password}"/>
        <property name="maxActive" value="20"/>
        <property name="maxIdle" value="5"/>
        <property name="maxWait" value="1000"/>
        <property name="defaultAutoCommit" value="true"/>
    </bean>
    
    <bean id="sqlGen" class="cn.com.talkweb.core.base.util.MysqlSqlGen" autowire="byName"></bean>
    <bean id="queryFactory" class="cn.com.talkweb.core.base.util.QueryFactory" autowire="byName">
    	
    </bean>
	<bean id="springInit" class="cn.com.talkweb.core.base.spring.SpringContextHolder"  ></bean>
	<context:component-scan base-package="${project.annotationPackage}" />
	<bean id="jdbcDao"  class="cn.com.talkweb.core.base.dao.JdbcDao"  autowire="byName" />
       
</beans>
