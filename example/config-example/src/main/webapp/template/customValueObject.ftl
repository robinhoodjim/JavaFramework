package ${class.modelPackage};

import java.io.Serializable;
import cn.com.talkweb.core.base.annotation.MappingEntity;
import cn.com.talkweb.core.base.annotation.MappingField;
import cn.com.talkweb.core.base.model.BaseObject;

/**
 * <p>Title:${project.projName}</p>
 *
 * <p>Description:${project.description}</p>
 *
 * <p>Usage:${class.name} 实体Model</p>
 *
 * <p>Company: ${project.company}</p>
 *
 * <p>Copyright: Copyright (c) ${CurYear}</p>
 *
 * <p>LastUpdate: ${LastUpdate}  By:${project.author} </p>
 *
 * This is an object that contains data related to the  table.
 * 
 * @author ${project.author}
 * @version 1.0
 */
 
@MappingEntity(schema="${class.dbschema}",table="${class.entityCode}")
public class ${class.upperName} extends BaseObject {
    private static final long serialVersionUID = 1L;
   // fields
<#list fields as prop>
	@MappingField(<#if (prop.isPrimary=='1')>primary="1",</#if><#if (prop.isGenkey=='1')>increment="1",</#if>field="${prop.field}")
   private ${prop.type} ${prop.code};   
</#list>

   // Constructors
   public ${class.upperName}() {
   }
<#list fields as prop>
   /**
    * Return the value associated with the column: 
    *
    */
   public ${prop.type} get${prop.uppername}() { 
      return this.${prop.code}; 
   }
   
   public void set${prop.uppername}(${prop.type} ${prop.code}) { 
      this.${prop.code} = ${prop.code}; 
   }
</#list>

  
   
   public boolean equals(Object obj) {
      if (null == obj) return false;
      if (!(obj instanceof ${class.modelPackage}.${class.upperName})) return false;
      else {
         ${class.modelPackage}.${class.upperName} o = (${class.modelPackage}.${class.upperName}) obj;
        <#list fields as prop>
        <#if (prop.isPrimary=='1')>
         if (null == this.get${prop.uppername}() || null == o.get${prop.uppername}()) return false;
         else return (this.get${prop.uppername}().equals(o.get${prop.uppername}()));
         </#if>
        </#list>
      }
   }
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[${class.upperName}:");
<#list fields as prop>
		buffer.append("${prop.code}:").append(dealNull(${prop.code}));
</#list>
		buffer.append("]");
		return buffer.toString();
   }

	private String dealNull(Object str) {
		if(str==null) return ""; else return str.toString();
	}
}