package ${class.servicePackage};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import cn.com.talkweb.core.base.service.BaseAnnotationJdbcService;

import ${class.modelPackage}.${class.upperName};

/**
 * <p>Project:  ${project.projName}</p>
 *
 * <p>Description:${project.description}</p>
 *
 * <p>Copyright: Copyright (c) ${CurYear}</p>
 *
 *<p>LastUpdate: ${LastUpdate}  By:${project.author} </p>
 *
 * <p>Company: ${project.projName}</p>
 *
 * @author ${project.author}
 * @version 1.0
 */
@Component(value="${class.springName}Service")
@Scope(value="singleton")
public class ${class.upperName}Service extends BaseAnnotationJdbcService<${class.upperName}, ${class.pkType}> {
	
}
