package ${class.modelPackage};

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import cn.com.talkweb.core.base.model.BaseObject;

/**
 * <p>Title:  ${project.projName}</p>
 *
 * <p>Description:${project.description}</p>
 *
 * <p>Usage:${class.name} 实体Model</p>
 *
 * <p>Company: ${project.company}</p>
 *
 * <p>Copyright: Copyright (c) ${CurYear}</p>
 *
 * <p>LastUpdate: ${LastUpdate}  By:${author} </p>
 *
 * This is an object that contains data related to the  table.
 * 
 * @author ${author}
 * @version 1.0
 */
 
@Entity
@Table(name="${class.dbschema}",schema="${class.entityCode}")
public class ${class.javaClass} extends BaseObject {
    private static final long serialVersionUID = 1L;

   // fields
<#list fields as prop>
	<#if (prop.isPrimary=='1')>@Id </#if><#if (prop.isGenkey=='1')>@GeneratedValue(strategy=GenerationType.IDENTITY)</#if> <#if (prop.isSequnce=='1')>@SequenceGenerator(sequenceName="${prop.seqName}")</#if>
   <#if (prop.isPrimary=='0')>@Column(name="${prop.field}")</#if>
   private ${prop.type} ${prop.code};   
</#list>

   // Constructors
   public ${class.javaClass}() {
   }
<#list fields as prop>
   /**
    * Return the value associated with the column: 
    *
    */
   public ${prop.type} get${prop.uppername}() { 
      return this.${prop.code}; 
   }
   
   public void set${prop.uppername}(${prop.type} ${prop.code}) { 
      this.${prop.code} = ${prop.code}; 
   }
</#list>

  
   
   public boolean equals(Object obj) {
      if (null == obj) return false;
      if (!(obj instanceof ${class.modelPackage}.${class.javaClass})) return false;
      else {
         ${class.modelPackage}.${class.javaClass} o = (${class.modelPackage}.${class.javaClass}) obj;
        <#list fields as prop>
        <#if (prop.isPrimary=='1')>
         if (null == this.get${prop.uppername}() || null == o.get${prop.uppername}()) return false;
         else return (this.get${prop.uppername}().equals(o.get${prop.uppername}()));
         </#if>
        </#list>
      }
   }
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("[${class.javaClass}:");
<#list fields as prop>
		buffer.append(" ${prop.name}:").append(dealNull(${prop.name}));
</#list>
		buffer.append("]");
		return buffer.toString();
   }

	private String dealNull(Object str) {
		if(str==null) return ""; else return str.toString();
	}
}