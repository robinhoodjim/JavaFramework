<?xml version="1.0"?>
<!DOCTYPE hibernate-mapping PUBLIC "-//Hibernate/Hibernate Mapping DTD//EN" "http://hibernate.sourceforge.net/hibernate-mapping-3.0.dtd" >

<hibernate-mapping package="${class.modelPackage}">
   <class name="${class.javaClass}" table="${class.entityCode}" >
      <meta attribute="sync-DAO">true</meta>
      <#list fields as prop>
      <#if (prop.isPrimary=='1')>
      <id name="${prop.code}" column="${prop.field}" type="${prop.type}" >
      	<generator class="${prop.type}"/>
      </id>
      <#else>
       <property name="${prop.code}" column="${prop.field}" type="${prop.type}" not-null="${prop.isNull}" length="${prop.length}" />
      </#if>
		</#list>

   </class>	
</hibernate-mapping>