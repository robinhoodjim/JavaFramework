function mxEditor(a) {
    this.actions = [];
    this.addActions();
    if (document.body != null) {
        this.cycleAttributeValues = [];
        this.popupHandler = new mxDefaultPopupMenu;
        this.undoManager = new mxUndoManager;
        this.graph = this.createGraph();
        this.toolbar = this.createToolbar();
        this.keyHandler = new mxDefaultKeyHandler(this);
        this.configure(a);
        this.graph.swimlaneIndicatorColorAttribute = this.cycleAttributeName;
        if (!mxClient.IS_LOCAL && this.urlInit != null) this.session = this.createSession();
        if (this.onInit != null) this.onInit();
        mxClient.IS_IE && mxEvent.addListener(window, "unload", mxUtils.bind(this,
        function() {
            this.destroy()
        }))
    }
}
mxLoadResources && mxResources.add(mxClient.basePath + "/resources/editor");
mxEditor.prototype = new mxEventSource;
mxEditor.prototype.constructor = mxEditor;
mxEditor.prototype.askZoomResource = "none" != mxClient.language ? "askZoom": "";
mxEditor.prototype.lastSavedResource = "none" != mxClient.language ? "lastSaved": "";
mxEditor.prototype.currentFileResource = "none" != mxClient.language ? "currentFile": "";
mxEditor.prototype.propertiesResource = "none" != mxClient.language ? "properties": "";
mxEditor.prototype.tasksResource = "none" != mxClient.language ? "tasks": "";
mxEditor.prototype.helpResource = "none" != mxClient.language ? "help": "";
mxEditor.prototype.outlineResource = "none" != mxClient.language ? "outline": "";
mxEditor.prototype.outline = null;
mxEditor.prototype.graph = null;
mxEditor.prototype.graphRenderHint = null;
mxEditor.prototype.toolbar = null;
mxEditor.prototype.session = null;
mxEditor.prototype.status = null;
mxEditor.prototype.popupHandler = null;
mxEditor.prototype.undoManager = null;
mxEditor.prototype.keyHandler = null;
mxEditor.prototype.actions = null;
mxEditor.prototype.dblClickAction = "edit";
mxEditor.prototype.swimlaneRequired = !1;
mxEditor.prototype.disableContextMenu = !0;
mxEditor.prototype.insertFunction = null;
mxEditor.prototype.forcedInserting = !1;
mxEditor.prototype.templates = null;
mxEditor.prototype.defaultEdge = null;
mxEditor.prototype.defaultEdgeStyle = null;
mxEditor.prototype.defaultGroup = null;
mxEditor.prototype.groupBorderSize = null;
mxEditor.prototype.filename = null;
mxEditor.prototype.linefeed = "&#xa;";
mxEditor.prototype.postParameterName = "xml";
mxEditor.prototype.escapePostData = !0;
mxEditor.prototype.urlPost = null;
mxEditor.prototype.urlImage = null;
mxEditor.prototype.urlInit = null;
mxEditor.prototype.urlNotify = null;
mxEditor.prototype.urlPoll = null;
mxEditor.prototype.horizontalFlow = !1;
mxEditor.prototype.layoutDiagram = !1;
mxEditor.prototype.swimlaneSpacing = 0;
mxEditor.prototype.maintainSwimlanes = !1;
mxEditor.prototype.layoutSwimlanes = !1;
mxEditor.prototype.cycleAttributeValues = null;
mxEditor.prototype.cycleAttributeIndex = 0;
mxEditor.prototype.cycleAttributeName = "fillColor";
mxEditor.prototype.tasks = null;
mxEditor.prototype.tasksWindowImage = null;
mxEditor.prototype.tasksTop = 20;
mxEditor.prototype.help = null;
mxEditor.prototype.helpWindowImage = null;
mxEditor.prototype.urlHelp = null;
mxEditor.prototype.helpWidth = 300;
mxEditor.prototype.helpHeight = 260;
mxEditor.prototype.propertiesWidth = 240;
mxEditor.prototype.propertiesHeight = null;
mxEditor.prototype.movePropertiesDialog = !1;
mxEditor.prototype.validating = !1;
mxEditor.prototype.modified = !1;
mxEditor.prototype.isModified = function() {
    return this.modified
};
mxEditor.prototype.setModified = function(a) {
    this.modified = a
};
mxEditor.prototype.addActions = function() {
    this.addAction("save",
    function(a) {
        a.save()
    });
    this.addAction("print",
    function(a) { (new mxPrintPreview(a.graph, 1)).open()
    });
    this.addAction("show",
    function(a) {
        mxUtils.show(a.graph, null, 10, 10)
    });
    this.addAction("exportImage",
    function(a) {
        var b = a.getUrlImage();
        if (b == null || mxClient.IS_LOCAL) a.execute("show");
        else {
            var c = mxUtils.getViewXml(a.graph, 1),
            c = mxUtils.getXml(c, "\n");
            mxUtils.submit(b, a.postParameterName + "=" + encodeURIComponent(c), document, "_blank")
        }
    });
    this.addAction("refresh",
    function(a) {
        a.graph.refresh()
    });
    this.addAction("cut",
    function(a) {
        a.graph.isEnabled() && mxClipboard.cut(a.graph)
    });
    this.addAction("copy",
    function(a) {
        a.graph.isEnabled() && mxClipboard.copy(a.graph)
    });
    this.addAction("paste",
    function(a) {
        a.graph.isEnabled() && mxClipboard.paste(a.graph)
    });
    this.addAction("delete",
    function(a) {
        a.graph.isEnabled() && a.graph.removeCells()
    });
    this.addAction("group",
    function(a) {
        a.graph.isEnabled() && a.graph.setSelectionCell(a.groupCells())
    });
    this.addAction("ungroup",
    function(a) {
        a.graph.isEnabled() && a.graph.setSelectionCells(a.graph.ungroupCells())
    });
    this.addAction("removeFromParent",
    function(a) {
        a.graph.isEnabled() && a.graph.removeCellsFromParent()
    });
    this.addAction("undo",
    function(a) {
        a.graph.isEnabled() && a.undo()
    });
    this.addAction("redo",
    function(a) {
        a.graph.isEnabled() && a.redo()
    });
    this.addAction("zoomIn",
    function(a) {
        a.graph.zoomIn()
    });
    this.addAction("zoomOut",
    function(a) {
        a.graph.zoomOut()
    });
    this.addAction("actualSize",
    function(a) {
        a.graph.zoomActual()
    });
    this.addAction("fit",
    function(a) {
        a.graph.fit()
    });
    this.addAction("showProperties",
    function(a, b) {
        a.showProperties(b)
    });
    this.addAction("selectAll",
    function(a) {
        a.graph.isEnabled() && a.graph.selectAll()
    });
    this.addAction("selectNone",
    function(a) {
        a.graph.isEnabled() && a.graph.clearSelection()
    });
    this.addAction("selectVertices",
    function(a) {
        a.graph.isEnabled() && a.graph.selectVertices()
    });
    this.addAction("selectEdges",
    function(a) {
        a.graph.isEnabled() && a.graph.selectEdges()
    });
    this.addAction("edit",
    function(a, b) {
        a.graph.isEnabled() && a.graph.isCellEditable(b) && a.graph.startEditingAtCell(b)
    });
    this.addAction("toBack",
    function(a) {
        a.graph.isEnabled() && a.graph.orderCells(true)
    });
    this.addAction("toFront",
    function(a) {
        a.graph.isEnabled() && a.graph.orderCells(false)
    });
    this.addAction("enterGroup",
    function(a, b) {
        a.graph.enterGroup(b)
    });
    this.addAction("exitGroup",
    function(a) {
        a.graph.exitGroup()
    });
    this.addAction("home",
    function(a) {
        a.graph.home()
    });
    this.addAction("selectPrevious",
    function(a) {
        a.graph.isEnabled() && a.graph.selectPreviousCell()
    });
    this.addAction("selectNext",
    function(a) {
        a.graph.isEnabled() && a.graph.selectNextCell()
    });
    this.addAction("selectParent",
    function(a) {
        a.graph.isEnabled() && a.graph.selectParentCell()
    });
    this.addAction("selectChild",
    function(a) {
        a.graph.isEnabled() && a.graph.selectChildCell()
    });
    this.addAction("collapse",
    function(a) {
        a.graph.isEnabled() && a.graph.foldCells(true)
    });
    this.addAction("collapseAll",
    function(a) {
        if (a.graph.isEnabled()) {
            var b = a.graph.getChildVertices();
            a.graph.foldCells(true, false, b)
        }
    });
    this.addAction("expand",
    function(a) {
        a.graph.isEnabled() && a.graph.foldCells(false)
    });
    this.addAction("expandAll",
    function(a) {
        if (a.graph.isEnabled()) {
            var b = a.graph.getChildVertices();
            a.graph.foldCells(false, false, b)
        }
    });
    this.addAction("bold",
    function(a) {
        a.graph.isEnabled() && a.graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_BOLD)
    });
    this.addAction("italic",
    function(a) {
        a.graph.isEnabled() && a.graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_ITALIC)
    });
    this.addAction("underline",
    function(a) {
        a.graph.isEnabled() && a.graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_UNDERLINE)
    });
    this.addAction("shadow",
    function(a) {
        a.graph.isEnabled() && a.graph.toggleCellStyleFlags(mxConstants.STYLE_FONTSTYLE, mxConstants.FONT_SHADOW)
    });
    this.addAction("alignCellsLeft",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_LEFT)
    });
    this.addAction("alignCellsCenter",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_CENTER)
    });
    this.addAction("alignCellsRight",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_RIGHT)
    });
    this.addAction("alignCellsTop",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_TOP)
    });
    this.addAction("alignCellsMiddle",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_MIDDLE)
    });
    this.addAction("alignCellsBottom",
    function(a) {
        a.graph.isEnabled() && a.graph.alignCells(mxConstants.ALIGN_BOTTOM)
    });
    this.addAction("alignFontLeft",
    function(a) {
        a.graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_LEFT)
    });
    this.addAction("alignFontCenter",
    function(a) {
        a.graph.isEnabled() && a.graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_CENTER)
    });
    this.addAction("alignFontRight",
    function(a) {
        a.graph.isEnabled() && a.graph.setCellStyles(mxConstants.STYLE_ALIGN, mxConstants.ALIGN_RIGHT)
    });
    this.addAction("alignFontTop",
    function(a) {
        a.graph.isEnabled() && a.graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_TOP)
    });
    this.addAction("alignFontMiddle",
    function(a) {
        a.graph.isEnabled() && a.graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_MIDDLE)
    });
    this.addAction("alignFontBottom",
    function(a) {
        a.graph.isEnabled() && a.graph.setCellStyles(mxConstants.STYLE_VERTICAL_ALIGN, mxConstants.ALIGN_BOTTOM)
    });
    this.addAction("zoom",
    function(a) {
        var b = a.graph.getView().scale * 100,
        b = parseFloat(mxUtils.prompt(mxResources.get(a.askZoomResource) || a.askZoomResource, b)) / 100;
        isNaN(b) || a.graph.getView().setScale(b)
    });
    this.addAction("toggleTasks",
    function(a) {
        a.tasks != null ? a.tasks.setVisible(!a.tasks.isVisible()) : a.showTasks()
    });
    this.addAction("toggleHelp",
    function(a) {
        a.help != null ? a.help.setVisible(!a.help.isVisible()) : a.showHelp()
    });
    this.addAction("toggleOutline",
    function(a) {
        a.outline == null ? a.showOutline() : a.outline.setVisible(!a.outline.isVisible())
    });
    this.addAction("toggleConsole",
    function() {
        mxLog.setVisible(!mxLog.isVisible())
    })
};
mxEditor.prototype.createSession = function() {
    return this.connect(this.urlInit, this.urlPoll, this.urlNotify, mxUtils.bind(this,
    function(a) {
        this.fireEvent(new mxEventObject(mxEvent.SESSION, "session", a))
    }))
};
mxEditor.prototype.configure = function(a) {
    if (a != null) { (new mxCodec(a.ownerDocument)).decode(a, this);
        this.resetHistory()
    }
};
mxEditor.prototype.resetFirstTime = function() {
    document.cookie = "mxgraph=seen; expires=Fri, 27 Jul 2001 02:47:11 UTC; path=/"
};
mxEditor.prototype.resetHistory = function() {
    this.lastSnapshot = (new Date).getTime();
    this.undoManager.clear();
    this.ignoredChanges = 0;
    this.setModified(false)
};
mxEditor.prototype.addAction = function(a, b) {
    this.actions[a] = b
};
mxEditor.prototype.execute = function(a, b, c) {
    var d = this.actions[a];
    if (d != null) try {
        var e = arguments;
        e[0] = this;
        d.apply(this, e)
    } catch(f) {
        mxUtils.error("Cannot execute " + a + ": " + f.message, 280, true);
        throw f;
    } else mxUtils.error("Cannot find action " + a, 280, true)
};
mxEditor.prototype.addTemplate = function(a, b) {
    this.templates[a] = b
};
mxEditor.prototype.getTemplate = function(a) {
    return this.templates[a]
};
mxEditor.prototype.createGraph = function() {
    var a = new mxGraph(null, null, this.graphRenderHint);
    a.setTooltips(true);
    a.setPanning(true);
    this.installDblClickHandler(a);
    this.installUndoHandler(a);
    this.installDrillHandler(a);
    this.installChangeHandler(a);
    this.installInsertHandler(a);
    a.panningHandler.factoryMethod = mxUtils.bind(this,
    function(a, c, d) {
        return this.createPopupMenu(a, c, d)
    });
    a.connectionHandler.factoryMethod = mxUtils.bind(this,
    function(a, c) {
        return this.createEdge(a, c)
    });
    this.createSwimlaneManager(a);
    this.createLayoutManager(a);
    return a
};
mxEditor.prototype.createSwimlaneManager = function(a) {
    a = new mxSwimlaneManager(a, false);
    a.isHorizontal = mxUtils.bind(this,
    function() {
        return this.horizontalFlow
    });
    a.isEnabled = mxUtils.bind(this,
    function() {
        return this.maintainSwimlanes
    });
    return a
};
mxEditor.prototype.createLayoutManager = function(a) {
    var b = new mxLayoutManager(a),
    c = this;
    b.getLayout = function(b) {
        var e = null,
        f = c.graph.getModel();
        if (f.getParent(b) != null) if (c.layoutSwimlanes && a.isSwimlane(b)) {
            if (c.swimlaneLayout == null) c.swimlaneLayout = c.createSwimlaneLayout();
            e = c.swimlaneLayout
        } else if (c.layoutDiagram && (a.isValidRoot(b) || f.getParent(f.getParent(b)) == null)) {
            if (c.diagramLayout == null) c.diagramLayout = c.createDiagramLayout();
            e = c.diagramLayout
        }
        return e
    };
    return b
};
mxEditor.prototype.setGraphContainer = function(a) {
    if (this.graph.container == null) {
        this.graph.init(a);
        this.rubberband = new mxRubberband(this.graph);
        this.disableContextMenu && mxEvent.disableContextMenu(a);
        mxClient.IS_IE && new mxDivResizer(a)
    }
};
mxEditor.prototype.installDblClickHandler = function(a) {
    a.addListener(mxEvent.DOUBLE_CLICK, mxUtils.bind(this,
    function(b, c) {
        var d = c.getProperty("cell");
        if (d != null && a.isEnabled() && this.dblClickAction != null) {
            this.execute(this.dblClickAction, d);
            c.consume()
        }
    }))
};
mxEditor.prototype.installUndoHandler = function(a) {
    var b = mxUtils.bind(this,
    function(a, b) {
        this.undoManager.undoableEditHappened(b.getProperty("edit"))
    });
    a.getModel().addListener(mxEvent.UNDO, b);
    a.getView().addListener(mxEvent.UNDO, b);
    b = function(b, d) {
        var e = d.getProperty("edit").changes;
        a.setSelectionCells(a.getSelectionCellsForChanges(e))
    };
    this.undoManager.addListener(mxEvent.UNDO, b);
    this.undoManager.addListener(mxEvent.REDO, b)
};
mxEditor.prototype.installDrillHandler = function(a) {
    var b = mxUtils.bind(this,
    function() {
        this.fireEvent(new mxEventObject(mxEvent.ROOT))
    });
    a.getView().addListener(mxEvent.DOWN, b);
    a.getView().addListener(mxEvent.UP, b)
};
mxEditor.prototype.installChangeHandler = function(a) {
    var b = mxUtils.bind(this,
    function(b, d) {
        this.setModified(true);
        this.validating == true && a.validateGraph();
        for (var e = d.getProperty("edit").changes, f = 0; f < e.length; f++) {
            var g = e[f];
            if (g instanceof mxRootChange || g instanceof mxValueChange && g.cell == this.graph.model.root || g instanceof mxCellAttributeChange && g.cell == this.graph.model.root) {
                this.fireEvent(new mxEventObject(mxEvent.ROOT));
                break
            }
        }
    });
    a.getModel().addListener(mxEvent.CHANGE, b)
};
mxEditor.prototype.installInsertHandler = function(a) {
    var b = this;
    a.addMouseListener({
        mouseDown: function(a, d) {
            if (b.insertFunction != null && !d.isPopupTrigger() && (b.forcedInserting || d.getState() == null)) {
                b.graph.clearSelection();
                b.insertFunction(d.getEvent(), d.getCell());
                this.isActive = true;
                d.consume()
            }
        },
        mouseMove: function(a, b) {
            this.isActive && b.consume()
        },
        mouseUp: function(a, b) {
            if (this.isActive) {
                this.isActive = false;
                b.consume()
            }
        }
    })
};
mxEditor.prototype.createDiagramLayout = function() {
    var a = this.graph.gridSize,
    b = new mxStackLayout(this.graph, !this.horizontalFlow, this.swimlaneSpacing, 2 * a, 2 * a);
    b.isVertexIgnored = function(a) {
        return ! b.graph.isSwimlane(a)
    };
    return b
};
mxEditor.prototype.createSwimlaneLayout = function() {
    return new mxCompactTreeLayout(this.graph, this.horizontalFlow)
};
mxEditor.prototype.createToolbar = function() {
    return new mxDefaultToolbar(null, this)
};
mxEditor.prototype.setToolbarContainer = function(a) {
    this.toolbar.init(a);
    mxClient.IS_IE && new mxDivResizer(a)
};
mxEditor.prototype.setStatusContainer = function(a) {
    if (this.status == null) {
        this.status = a;
        this.addListener(mxEvent.SAVE, mxUtils.bind(this,
        function() {
            var a = (new Date).toLocaleString();
            this.setStatus((mxResources.get(this.lastSavedResource) || this.lastSavedResource) + ": " + a)
        }));
        this.addListener(mxEvent.OPEN, mxUtils.bind(this,
        function() {
            this.setStatus((mxResources.get(this.currentFileResource) || this.currentFileResource) + ": " + this.filename)
        }));
        mxClient.IS_IE && new mxDivResizer(a)
    }
};
mxEditor.prototype.setStatus = function(a) {
    if (this.status != null && a != null) this.status.innerHTML = a
};
mxEditor.prototype.setTitleContainer = function(a) {
    this.addListener(mxEvent.ROOT, mxUtils.bind(this,
    function() {
        a.innerHTML = this.getTitle()
    }));
    mxClient.IS_IE && new mxDivResizer(a)
};
mxEditor.prototype.treeLayout = function(a, b) {
    a != null && (new mxCompactTreeLayout(this.graph, b)).execute(a)
};
mxEditor.prototype.getTitle = function() {
    for (var a = "",
    b = this.graph,
    c = b.getCurrentRoot(); c != null && b.getModel().getParent(b.getModel().getParent(c)) != null;) {
        b.isValidRoot(c) && (a = " > " + b.convertValueToString(c) + a);
        c = b.getModel().getParent(c)
    }
    return this.getRootTitle() + a
};
mxEditor.prototype.getRootTitle = function() {
    return this.graph.convertValueToString(this.graph.getModel().getRoot())
};
mxEditor.prototype.undo = function() {
    this.undoManager.undo()
};
mxEditor.prototype.redo = function() {
    this.undoManager.redo()
};
mxEditor.prototype.groupCells = function() {
    var a = this.groupBorderSize != null ? this.groupBorderSize: this.graph.gridSize;
    return this.graph.groupCells(this.createGroup(), a)
};
mxEditor.prototype.createGroup = function() {
    return this.graph.getModel().cloneCell(this.defaultGroup)
};
mxEditor.prototype.open = function(a) {
    if (a != null) {
        this.readGraphModel(mxUtils.load(a).getXml().documentElement);
        this.filename = a;
        this.fireEvent(new mxEventObject(mxEvent.OPEN, "filename", a))
    }
};
mxEditor.prototype.readGraphModel = function(a) { (new mxCodec(a.ownerDocument)).decode(a, this.graph.getModel());
    this.resetHistory()
};
mxEditor.prototype.save = function(a, b) {
    a = a || this.getUrlPost();
    if (a != null && a.length > 0) {
        var c = this.writeGraphModel(b);
        this.postDiagram(a, c);
        this.setModified(false)
    }
    this.fireEvent(new mxEventObject(mxEvent.SAVE, "url", a))
};
mxEditor.prototype.postDiagram = function(a, b) {
    this.escapePostData && (b = encodeURIComponent(b));
    mxUtils.post(a, this.postParameterName + "=" + b, mxUtils.bind(this,
    function(c) {
        this.fireEvent(new mxEventObject(mxEvent.POST, "request", c, "url", a, "data", b))
    }))
};
mxEditor.prototype.writeGraphModel = function(a) {
    var a = a != null ? a: this.linefeed,
    b = (new mxCodec).encode(this.graph.getModel());
    return mxUtils.getXml(b, a)
};
mxEditor.prototype.getUrlPost = function() {
    return this.urlPost
};
mxEditor.prototype.getUrlImage = function() {
    return this.urlImage
};
mxEditor.prototype.connect = function(a, b, c, d) {
    var e = null;
    if (!mxClient.IS_LOCAL) {
        e = new mxSession(this.graph.getModel(), a, b, c);
        e.addListener(mxEvent.RECEIVE, mxUtils.bind(this,
        function(a, b) {
            b.getProperty("node").getAttribute("namespace") != null && this.resetHistory()
        }));
        e.addListener(mxEvent.DISCONNECT, d);
        e.addListener(mxEvent.CONNECT, d);
        e.addListener(mxEvent.NOTIFY, d);
        e.addListener(mxEvent.GET, d);
        e.start()
    }
    return e
};
mxEditor.prototype.swapStyles = function(a, b) {
    var c = this.graph.getStylesheet().styles[b];
    this.graph.getView().getStylesheet().putCellStyle(b, this.graph.getStylesheet().styles[a]);
    this.graph.getStylesheet().putCellStyle(a, c);
    this.graph.refresh()
};
mxEditor.prototype.showProperties = function(a) {
    a = a || this.graph.getSelectionCell();
    if (a == null) {
        a = this.graph.getCurrentRoot();
        a == null && (a = this.graph.getModel().getRoot())
    }
    if (a != null) {
        this.graph.stopEditing(true);
        var b = mxUtils.getOffset(this.graph.container),
        c = b.x + 10,
        b = b.y;
        if (this.properties != null && !this.movePropertiesDialog) {
            c = this.properties.getX();
            b = this.properties.getY()
        } else {
            var d = this.graph.getCellBounds(a);
            if (d != null) {
                c = c + (d.x + Math.min(200, d.width));
                b = b + d.y
            }
        }
        this.hideProperties();
        a = this.createProperties(a);
        if (a != null) {
            this.properties = new mxWindow(mxResources.get(this.propertiesResource) || this.propertiesResource, a, c, b, this.propertiesWidth, this.propertiesHeight, false);
            this.properties.setVisible(true)
        }
    }
};
mxEditor.prototype.isPropertiesVisible = function() {
    return this.properties != null
};
mxEditor.prototype.createProperties = function(a) {
    var b = this.graph.getModel(),
    c = b.getValue(a);
    if (mxUtils.isNode(c)) {
        var d = new mxForm("properties");
        d.addText("ID", a.getId()).setAttribute("readonly", "true");
        var e = null,
        f = null,
        g = null,
        h = null,
        k = null;
        if (b.isVertex(a)) {
            e = b.getGeometry(a);
            if (e != null) {
                f = d.addText("top", e.y);
                g = d.addText("left", e.x);
                h = d.addText("width", e.width);
                k = d.addText("height", e.height)
            }
        }
        for (var i = b.getStyle(a), l = d.addText("Style", i || ""), m = c.attributes, n = [], c = 0; c < m.length; c++) n[c] = d.addTextarea(m[c].nodeName, m[c].nodeValue, m[c].nodeName == "label" ? 4 : 2);
        c = mxUtils.bind(this,
        function() {
            this.hideProperties();
            b.beginUpdate();
            try {
                if (e != null) {
                    e = e.clone();
                    e.x = parseFloat(g.value);
                    e.y = parseFloat(f.value);
                    e.width = parseFloat(h.value);
                    e.height = parseFloat(k.value);
                    b.setGeometry(a, e)
                }
                l.value.length > 0 ? b.setStyle(a, l.value) : b.setStyle(a, null);
                for (var c = 0; c < m.length; c++) {
                    var d = new mxCellAttributeChange(a, m[c].nodeName, n[c].value);
                    b.execute(d)
                }
                this.graph.isAutoSizeCell(a) && this.graph.updateCellSize(a)
            } finally {
                b.endUpdate()
            }
        });
        i = mxUtils.bind(this,
        function() {
            this.hideProperties()
        });
        d.addButtons(c, i);
        return d.table
    }
    return null
};
mxEditor.prototype.hideProperties = function() {
    if (this.properties != null) {
        this.properties.destroy();
        this.properties = null
    }
};
mxEditor.prototype.showTasks = function() {
    if (this.tasks == null) {
        var a = document.createElement("div");
        a.style.padding = "4px";
        a.style.paddingLeft = "20px";
        var b = document.body.clientWidth,
        b = new mxWindow(mxResources.get(this.tasksResource) || this.tasksResource, a, b - 220, this.tasksTop, 200);
        b.setClosable(true);
        b.destroyOnClose = false;
        var c = mxUtils.bind(this,
        function() {
            mxEvent.release(a);
            a.innerHTML = "";
            this.createTasks(a)
        });
        this.graph.getModel().addListener(mxEvent.CHANGE, c);
        this.graph.getSelectionModel().addListener(mxEvent.CHANGE, c);
        this.graph.addListener(mxEvent.ROOT, c);
        this.tasksWindowImage != null && b.setImage(this.tasksWindowImage);
        this.tasks = b;
        this.createTasks(a)
    }
    this.tasks.setVisible(true)
};
mxEditor.prototype.refreshTasks = function(a) {
    if (this.tasks != null) {
        a = this.tasks.content;
        mxEvent.release(a);
        a.innerHTML = "";
        this.createTasks(a)
    }
};
mxEditor.prototype.createTasks = function() {};
mxEditor.prototype.showHelp = function() {
    if (this.help == null) {
        var a = document.createElement("iframe");
        a.setAttribute("src", mxResources.get("urlHelp") || this.urlHelp);
        a.setAttribute("height", "100%");
        a.setAttribute("width", "100%");
        a.setAttribute("frameBorder", "0");
        a.style.backgroundColor = "white";
        var b = document.body.clientWidth,
        c = document.body.clientHeight || document.documentElement.clientHeight,
        d = new mxWindow(mxResources.get(this.helpResource) || this.helpResource, a, (b - this.helpWidth) / 2, (c - this.helpHeight) / 3, this.helpWidth, this.helpHeight);
        d.setMaximizable(true);
        d.setClosable(true);
        d.destroyOnClose = false;
        d.setResizable(true);
        this.helpWindowImage != null && d.setImage(this.helpWindowImage);
        if (mxClient.IS_NS) {
            b = function() {
                a.setAttribute("height", d.div.offsetHeight - 26 + "px")
            };
            d.addListener(mxEvent.RESIZE_END, b);
            d.addListener(mxEvent.MAXIMIZE, b);
            d.addListener(mxEvent.NORMALIZE, b);
            d.addListener(mxEvent.SHOW, b)
        }
        this.help = d
    }
    this.help.setVisible(true)
};
mxEditor.prototype.showOutline = function() {
    if (this.outline == null) {
        var a = document.createElement("div");
        a.style.overflow = "hidden";
        a.style.width = "100%";
        a.style.height = "100%";
        a.style.background = "white";
        a.style.cursor = "move";
        var b = new mxWindow(mxResources.get(this.outlineResource) || this.outlineResource, a, 600, 480, 200, 200, false),
        c = new mxOutline(this.graph, a);
        b.setClosable(true);
        b.setResizable(true);
        b.destroyOnClose = false;
        b.addListener(mxEvent.RESIZE_END,
        function() {
            c.update()
        });
        this.outline = b;
        this.outline.outline = c
    }
    this.outline.setVisible(true);
    this.outline.outline.update(true)
};
mxEditor.prototype.setMode = function(a) {
    if (a == "select") {
        this.graph.panningHandler.useLeftButtonForPanning = false;
        this.graph.setConnectable(false)
    } else if (a == "connect") {
        this.graph.panningHandler.useLeftButtonForPanning = false;
        this.graph.setConnectable(true)
    } else if (a == "pan") {
        this.graph.panningHandler.useLeftButtonForPanning = true;
        this.graph.setConnectable(false)
    }
};
mxEditor.prototype.createPopupMenu = function(a, b, c) {
    this.popupHandler.createMenu(this, a, b, c)
};
mxEditor.prototype.createEdge = function() {
    var a = null;
    if (this.defaultEdge != null) a = this.graph.getModel().cloneCell(this.defaultEdge);
    else {
        a = new mxCell("");
        a.setEdge(true);
        var b = new mxGeometry;
        b.relative = true;
        a.setGeometry(b)
    }
    b = this.getEdgeStyle();
    b != null && a.setStyle(b);
    return a
};
mxEditor.prototype.getEdgeStyle = function() {
    return this.defaultEdgeStyle
};
mxEditor.prototype.consumeCycleAttribute = function(a) {
    return this.cycleAttributeValues != null && this.cycleAttributeValues.length > 0 && this.graph.isSwimlane(a) ? this.cycleAttributeValues[this.cycleAttributeIndex++%this.cycleAttributeValues.length] : null
};
mxEditor.prototype.cycleAttribute = function(a) {
    if (this.cycleAttributeName != null) {
        var b = this.consumeCycleAttribute(a);
        b != null && a.setStyle(a.getStyle() + ";" + this.cycleAttributeName + "=" + b)
    }
};
mxEditor.prototype.addVertex = function(a, b, c, d) {
    for (var e = this.graph.getModel(); a != null && !this.graph.isValidDropTarget(a);) a = e.getParent(a);
    var a = a != null ? a: this.graph.getSwimlaneAt(c, d),
    f = this.graph.getView().scale,
    g = e.getGeometry(b),
    h = e.getGeometry(a);
    if (this.graph.isSwimlane(b) && !this.graph.swimlaneNesting) a = null;
    else {
        if (a == null && this.swimlaneRequired) return null;
        if (a != null && h != null) {
            var k = this.graph.getView().getState(a);
            if (k != null) {
                c = c - k.origin.x * f;
                d = d - k.origin.y * f;
                if (this.graph.isConstrainedMoving) {
                    var h = g.width,
                    i = g.height,
                    l = k.x + k.width;
                    c + h > l && (c = c - (c + h - l));
                    l = k.y + k.height;
                    d + i > l && (d = d - (d + i - l))
                }
            } else if (h != null) {
                c = c - h.x * f;
                d = d - h.y * f
            }
        }
    }
    g = g.clone();
    g.x = this.graph.snap(c / f - this.graph.getView().translate.x - this.graph.gridSize / 2);
    g.y = this.graph.snap(d / f - this.graph.getView().translate.y - this.graph.gridSize / 2);
    b.setGeometry(g);
    a == null && (a = this.graph.getDefaultParent());
    this.cycleAttribute(b);
    this.fireEvent(new mxEventObject(mxEvent.BEFORE_ADD_VERTEX, "vertex", b, "parent", a));
    e.beginUpdate();
    try {
        b = this.graph.addCell(b, a);
        if (b != null) {
            this.graph.constrainChild(b);
            this.fireEvent(new mxEventObject(mxEvent.ADD_VERTEX, "vertex", b))
        }
    } finally {
        e.endUpdate()
    }
    if (b != null) {
        this.graph.setSelectionCell(b);
        this.graph.scrollCellToVisible(b);
        this.fireEvent(new mxEventObject(mxEvent.AFTER_ADD_VERTEX, "vertex", b))
    }
    return b
};
mxEditor.prototype.destroy = function() {
    if (!this.destroyed) {
        this.destroyed = true;
        this.tasks != null && this.tasks.destroy();
        this.outline != null && this.outline.destroy();
        this.properties != null && this.properties.destroy();
        this.keyHandler != null && this.keyHandler.destroy();
        this.rubberband != null && this.rubberband.destroy();
        this.toolbar != null && this.toolbar.destroy();
        this.graph != null && this.graph.destroy();
        this.templates = this.status = null
    }
};