function mxDefaultToolbar(a, b) {
    this.editor = b;
    a != null && b != null && this.init(a)
}
mxDefaultToolbar.prototype.editor = null;
mxDefaultToolbar.prototype.toolbar = null;
mxDefaultToolbar.prototype.resetHandler = null;
mxDefaultToolbar.prototype.spacing = 4;
mxDefaultToolbar.prototype.connectOnDrop = !1;
mxDefaultToolbar.prototype.init = function(a) {
    if (a != null) {
        this.toolbar = new mxToolbar(a);
        this.toolbar.addListener(mxEvent.SELECT, mxUtils.bind(this,
        function(a, c) {
            var d = c.getProperty("function");
            this.editor.insertFunction = d != null ? mxUtils.bind(this,
            function() {
                d.apply(this, arguments);
                this.toolbar.resetMode()
            }) : null
        }));
        this.resetHandler = mxUtils.bind(this,
        function() {
            this.toolbar != null && this.toolbar.resetMode(true)
        });
        this.editor.graph.addListener(mxEvent.DOUBLE_CLICK, this.resetHandler);
        this.editor.addListener(mxEvent.ESCAPE, this.resetHandler)
    }
};
mxDefaultToolbar.prototype.addItem = function(a, b, c, d) {
    var e = mxUtils.bind(this,
    function() {
        c != null && c.length > 0 && this.editor.execute(c)
    });
    return this.toolbar.addItem(a, b, e, d)
};
mxDefaultToolbar.prototype.addSeparator = function(a) {
    a = a || mxClient.imageBasePath + "/separator.gif";
    this.toolbar.addSeparator(a)
};
mxDefaultToolbar.prototype.addCombo = function() {
    return this.toolbar.addCombo()
};
mxDefaultToolbar.prototype.addActionCombo = function(a) {
    return this.toolbar.addActionCombo(a)
};
mxDefaultToolbar.prototype.addActionOption = function(a, b, c) {
    var d = mxUtils.bind(this,
    function() {
        this.editor.execute(c)
    });
    this.addOption(a, b, d)
};
mxDefaultToolbar.prototype.addOption = function(a, b, c) {
    return this.toolbar.addOption(a, b, c)
};
mxDefaultToolbar.prototype.addMode = function(a, b, c, d, e) {
    var f = mxUtils.bind(this,
    function() {
        this.editor.setMode(c);
        e != null && e(this.editor)
    });
    return this.toolbar.addSwitchMode(a, b, f, d)
};
mxDefaultToolbar.prototype.addPrototype = function(a, b, c, d, e, f) {
    var g = function() {
        return typeof c == "function" ? c() : c != null ? c.clone() : null
    },
    h = mxUtils.bind(this,
    function(a, b) {
        typeof e == "function" ? e(this.editor, g(), a, b) : this.drop(g(), a, b);
        this.toolbar.resetMode();
        mxEvent.consume(a)
    }),
    a = this.toolbar.addMode(a, b, h, d, null, f);
    this.installDropHandler(a,
    function(a, b, c) {
        h(b, c)
    });
    return a
};
mxDefaultToolbar.prototype.drop = function(a, b, c) {
    var d = this.editor.graph,
    e = d.getModel();
    if (c == null || e.isEdge(c) || !this.connectOnDrop || !d.isCellConnectable(c)) {
        for (; c != null && !d.isValidDropTarget(c, [a], b);) c = e.getParent(c);
        this.insert(a, b, c)
    } else this.connect(a, b, c)
};
mxDefaultToolbar.prototype.insert = function(a, b, c) {
    var d = this.editor.graph;
    if (d.canImportCell(a)) {
        var e = mxEvent.getClientX(b),
        f = mxEvent.getClientY(b),
        e = mxUtils.convertPoint(d.container, e, f);
        return d.isSplitEnabled() && d.isSplitTarget(c, [a], b) ? d.splitEdge(c, [a], null, e.x, e.y) : this.editor.addVertex(c, a, e.x, e.y)
    }
    return null
};
mxDefaultToolbar.prototype.connect = function(a, b, c) {
    var b = this.editor.graph,
    d = b.getModel();
    if (c != null && b.isCellConnectable(a) && b.isEdgeValid(null, c, a)) {
        var e = null;
        d.beginUpdate();
        try {
            var f = d.getGeometry(c),
            g = d.getGeometry(a).clone();
            g.x = f.x + (f.width - g.width) / 2;
            g.y = f.y + (f.height - g.height) / 2;
            var h = this.spacing * b.gridSize,
            k = d.getDirectedEdgeCount(c, true) * 20;
            this.editor.horizontalFlow ? g.x = g.x + ((g.width + f.width) / 2 + h + k) : g.y = g.y + ((g.height + f.height) / 2 + h + k);
            a.setGeometry(g);
            var i = d.getParent(c);
            b.addCell(a, i);
            b.constrainChild(a);
            e = this.editor.createEdge(c, a);
            if (d.getGeometry(e) == null) {
                var l = new mxGeometry;
                l.relative = true;
                d.setGeometry(e, l)
            }
            b.addEdge(e, i, c, a)
        } finally {
            d.endUpdate()
        }
        b.setSelectionCells([a, e]);
        b.scrollCellToVisible(a)
    }
};
mxDefaultToolbar.prototype.installDropHandler = function(a, b) {
    var c = document.createElement("img");
    c.setAttribute("src", a.getAttribute("src"));
    var d = mxUtils.bind(this,
    function() {
        c.style.width = 2 * a.offsetWidth + "px";
        c.style.height = 2 * a.offsetHeight + "px";
        mxUtils.makeDraggable(a, this.editor.graph, b, c);
        mxEvent.removeListener(c, "load", d)
    });
    mxClient.IS_IE ? d() : mxEvent.addListener(c, "load", d)
};
mxDefaultToolbar.prototype.destroy = function() {
    if (this.resetHandler != null) {
        this.editor.graph.removeListener("dblclick", this.resetHandler);
        this.editor.removeListener("escape", this.resetHandler);
        this.resetHandler = null
    }
    if (this.toolbar != null) {
        this.toolbar.destroy();
        this.toolbar = null
    }
};