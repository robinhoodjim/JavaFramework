package com.robin.webui.contorller.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/test")
public class TestContorller {
    @RequestMapping("/sam1")
    public void doResponse(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("<script>alert('111');window.location='index.html'</scipt>");
    }
}
