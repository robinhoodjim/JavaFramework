package com.robin.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestPathRecursive {
    public static void main(String[] args){
        String relativeName="/tmp/acc/ree/rtt";
        String[] pathSeq=relativeName.split("/",-1);
        List<String> retList=new ArrayList<>();
        StringBuilder builder=new StringBuilder();
        builder.append("/").append(pathSeq[1]);
        if(pathSeq.length>2) {
            for (int i = 2; i<pathSeq.length;i++){
                builder.append("/").append(pathSeq[i]);
                retList.add(builder.toString());
            }
        }
        System.out.println(retList);
    }
}
