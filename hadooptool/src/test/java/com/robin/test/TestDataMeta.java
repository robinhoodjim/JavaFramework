package com.robin.test;

import com.robin.core.base.dao.SimpleJdbcDao;
import com.robin.core.base.datameta.*;
import com.robin.core.base.util.Const;
import com.robin.core.base.util.ResourceConst;
import com.robin.core.fileaccess.iterator.AbstractFileIterator;
import com.robin.core.fileaccess.iterator.TextFileIteratorFactory;
import com.robin.core.fileaccess.meta.DataCollectionMeta;
import com.robin.core.fileaccess.writer.AbstractFileWriter;
import com.robin.core.fileaccess.writer.TextFileWriterFactory;
import com.robin.core.query.extractor.ResultSetOperationExtractor;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class TestDataMeta extends TestCase {
    @Test
    public void testMetaGet(){
        DataBaseParam param=new DataBaseParam("10.1.0.168",0,"testdb","root","123456");
        BaseDataBaseMeta meta= DataBaseMetaFactory.getDataBaseMetaByType(BaseDataBaseMeta.TYPE_HIVE2,param);
        DataBaseUtil util=new DataBaseUtil();

        try(Connection connection= SimpleJdbcDao.getConnection(meta)){
            //util.connect(meta);
            //List list=util.getTableMetaByTableName("hql_audit_log","testdb");
            //assertNotNull(list);
            List<Map<String,Object>> list1=SimpleJdbcDao.queryBySqlNoMeta(connection,"select * from testdb.monitor_process_trace where status=? limit 10",new Object[]{"1"} );
            assertNotNull(list1);
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            if(util!=null){
                util.closeConnection();
            }
        }
    }
    @Test
    public void testOrcRead(){
        DataCollectionMeta colmeta = new DataCollectionMeta();
        colmeta.setResType(ResourceConst.ResourceType.TYPE_LOCALFILE.getValue());
        colmeta.setSourceType(ResourceConst.InputSourceType.TYPE_LOCAL.getValue());
        colmeta.setFileFormat(Const.FILETYPE_ORC);
        colmeta.setPath("file:///E:/2.zlib.orc");
        colmeta.setSourceType(ResourceConst.InputSourceType.TYPE_LOCAL.getValue());
        try {
            AbstractFileIterator iterator = TextFileIteratorFactory.getProcessIteratorByType(colmeta);
            while (iterator.hasNext()){
                System.out.println(iterator.next());
            }
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
    @Test
    public void testOrcWrite(){
        DataBaseParam param=new DataBaseParam("10.1.1.189",0,"isc_test","root","123456");
        BaseDataBaseMeta meta=DataBaseMetaFactory.getDataBaseMetaByType(BaseDataBaseMeta.TYPE_MYSQL,param);

        try(Connection connection=SimpleJdbcDao.getConnection(meta)){
            List<DataBaseColumnMeta> columnMetas= DataBaseUtil.getTableMetaByTableName(connection,"r30_ws_approve","isc_test",meta.getDbType());
            DataCollectionMeta colmeta=new DataCollectionMeta();
            colmeta.setSourceType(ResourceConst.ResourceType.TYPE_LOCALFILE.getValue());
            colmeta.setFileFormat(Const.FILETYPE_ORC);
            colmeta.setSourceType(ResourceConst.InputSourceType.TYPE_LOCAL.getValue());
            colmeta.setPath("file:///e:/2.zlib.orc");
            colmeta.setColumnList(DataCollectionMeta.parseMeta(columnMetas));
            AbstractFileWriter writer= TextFileWriterFactory.getFileOutputStreamByType(Const.FILESUFFIX_ORC,colmeta,null);
            writer.beginWrite();
            SimpleJdbcDao.executeOperationWithQuery(connection, "select * from r30_ws_approve", new Object[]{}, false, new ResultSetOperationExtractor() {
                @Override
                public boolean executeAdditionalOperation(Map<String, Object> map, ResultSetMetaData rsmd) throws SQLException {
                    try{
                        writer.writeRecord(map);
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                    return true;
                }
            });
            writer.finishWrite();

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }
}
